package com.iitb.tspILProuting;

import java.io.File;
import java.util.Vector;

import com.iiitb.scm.quadraticSolver.CPLEXILPFile;

public class TSPILPRun {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TSPILPDataPreprocessing runObject = new TSPILPDataPreprocessing();
		//runObject.dataExtraction("9708YP(26)", "S1",13.02886,
				//77.5335698,true);
		runObject.dataExtraction("1531YP(22)", "S3",13.02886,
				77.5335698,true);
		//runObject.dataExtraction("9708YP(26)", "S2",13.02886,
				//77.5335698,false);
		runObject.dataExtraction("1531YP(22)", "S4",13.02886,
				77.5335698,false);
		Vector nodes = runObject.getNodes();
		Vector edges = runObject.getEdges();
		TSPILPConstraints tc = new TSPILPConstraints();
		tc.populateInputNetwork(nodes,edges,runObject.numNodesNW1,runObject.numNodesNW2);
		tc.createILPModel();
		//Create CPLEX file to solve ILP
		CPLEXILPFile.createLPFile(tc.getObjective(), tc.getObjectiveType(), tc.getConstraints(), 
				tc.getILPVariables(), null, new File(System.getProperty("user.dir")+"\\output\\BBRouting.lp"));
	}
	
	
/*	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		for( int k=0;k <1; k++)
		{
			TSPILPDataPreprocessing runObject = new TSPILPDataPreprocessing();
			runObject.dataExtraction("1531YP(22)", "S1",13.02886,
					77.5335698,true,"S2_"+k);
			runObject.dataExtraction("1531YP(22)", "S2",13.02886,
					77.5335698,false,"S2_"+k);
			Vector nodes = runObject.getNodes();
			Vector edges = runObject.getEdges();
			TSPILPConstraints tc = new TSPILPConstraints();
			tc.populateInputNetwork(nodes,edges,runObject.numNodesNW1,runObject.numNodesNW2,"S2_"+k);
			tc.createILPModel();
			//Create CPLEX file to solve ILP
			CPLEXILPFile.createLPFile(tc.getObjective(), tc.getObjectiveType(), tc.getConstraints(), 
					tc.getILPVariables(), null, new File(System.getProperty("user.dir")+"\\output\\BBRouting_"+k+".lp"));
			
		}
		
	}*/

}
