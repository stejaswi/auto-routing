/** 
 * QuadMain2: Testing CplexQPSolver with a set of LP format files
 * @author Shreyas B.G.
 * @version 0.1
 * Creation Date  23-04-2007
 * Copyright IIITB
 */

/* History: 
 * ---------------------------------------------------------------------
 *      Date  |        Description of Change          |    Author      |
 * ---------------------------------------------------------------------
 *  23-04-07  |          Creation of File             |  Shreyas B.G.  |
 * ---------------------------------------------------------------------      
 */

package com.iiitb.scm.quadraticSolver;

import com.iiitb.scm.quadraticSolver.CplexQPSolver.SCMCplexException;

public class QuadMain2 {
	/**
	 * main() function used to test CplexQPSolver with Domain Tests
	 * @param args
	 * @return 
	 * @throws SCMCplexException
	 */
	
	
	public static void main(String[] args) throws SCMCplexException
	{
		try {

			String filename1 = "lpfile.lp";
			//String filename2 = "C:\\Documents and Settings\\shreyas\\Desktop\\domain\\test2.lp";
			//String filename3 = "C:\\Documents and Settings\\shreyas\\Desktop\\domain\\test3.lp";
			
			
			// Create quadratic solver object 1 and solve it 
			CplexQPSolver quadSolver1 = new CplexQPSolver(filename1,0);
			quadSolver1.cplexSolve();

            // Create quadratic solver object 2 and solve it
			//CplexQPSolver quadSolver2 = new CplexQPSolver(filename2);
    		//quadSolver2.cplexSolve();
			
            // Create quadratic solver object 3 and solve it    		
			//CplexQPSolver quadSolver3 = new CplexQPSolver(filename3);
			//quadSolver3.cplexSolve();			

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}// End of main()

	
}// End of QuadMain2 class