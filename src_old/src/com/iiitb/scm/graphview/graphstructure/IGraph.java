/*
 * Created on Jul 14, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.iiitb.scm.graphview.graphstructure;
import java.util.Vector;
/**
 * @author Harjinder
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
@SuppressWarnings("unchecked")
public interface IGraph {
	//This API is used by the Graph View....
	public Node addNode(int x, int y, String strColor);
	public int addNode(int x, int y, Attribute attr);
	public int addNode(int x, int y, int ID, Attribute attr);
	public Node getNodeAround(int x, int y);
	public void delNode(int x, int y);
	public void resetNodeXY(Node n);
	
	public Edge addEdge(Node nodeA, Node nodeB);
	public Edge getEdgeAround(int x, int y);
	public void delEdge(int x, int y);

	
	public Vector getNodesXY();
	public Vector getEdgesXY();
	public void report();
	
	//The API for the User Side....
	public Vector getNodeList();
	public Vector getEdgeList();
	public Vector getNodesWithAttributes(String attrName);
	public Vector getEdgesWithAttributes(String attrName);

	public Vector getNodesWithAttrValue(String attrName, String strValue);
	public Vector getEdgesWithAttrValue(String attrName, String strValue);
	
	// Following added by Sid for incorporating normal graphs in SCM package
	public int getGraphType();
	public void setGraphType(int graphType);
	
	// Added by Sid for Time Thing Once and for all ;)
	public int getMaxTime();
	public void setMaxTime(int maxTime);
	public int getMinTime();
	public void setMinTime(int minTime);
}
