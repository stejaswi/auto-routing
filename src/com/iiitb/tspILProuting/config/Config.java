package com.iiitb.tspILProuting.config;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;


public class Config {

	public static String SOLVER_PATH;
	public static void loadConfig()
	{
		Properties prop = new Properties();
		InputStream input = null;
		try
		{
			input = new FileInputStream("config//config.properties");

			// load a properties file
			prop.load(input);

			// get the property value
			SOLVER_PATH = prop.getProperty("solverPath");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
}
