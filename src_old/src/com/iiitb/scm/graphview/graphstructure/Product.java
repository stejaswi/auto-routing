package com.iiitb.scm.graphview.graphstructure;
/**

 * FileName: Product.java

 * @author Abhilasha

 * @date of creation June 17, 2014

 * @Copyright: International Institute of Information Technology, Bangalore, India (IIITB). All rights Reserved.

 * @description: This class models a product flowing through the supply chain. Each object of this class is a product

 *

 */
public class Product {

	private Attribute Attr = null;
	final static int DAILY = 1;
	final static int WEEKLY = 2;
	final static int MONTHLY = 3;
	final static int ANNUALLY = 4;
	
	private int frequency = 0;
	
	public Product(){
		Attr = null;
		frequency = 0;
	}
	
	public int getFrequency(){
		return this.frequency;
	}
	
	public void setFrequancy(int frequency){
		this.frequency = frequency;
	}
	
	public Attribute getAttribute(){
		return this.Attr;
	}
	
	public void setAttributes(Attribute attr){
		this.Attr = attr;
	}
	
	public void addAttribute(String strName, String strValue){
		this.Attr.addAttribute(strName, strValue);
	}	
}
