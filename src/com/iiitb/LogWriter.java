package com.iiitb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class LogWriter {

	static BufferedWriter logWriter = null;
	
	public static void initializeLog(File logFileName)
	{
		try
		{
			logWriter = new BufferedWriter(new FileWriter(logFileName));
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static void write(String logData){
		
		try
		{
			logWriter.write(logData);
			logWriter.newLine();
		}
		catch(Exception a){
			a.printStackTrace();
		}
		
	}
	
	public static void close()
	{
		try
		{
			if(logWriter!=null)
			{
				logWriter.flush();
				logWriter.close();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
}
