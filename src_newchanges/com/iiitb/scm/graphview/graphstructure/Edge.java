/*
 * Created on Jul 14, 2005
 */
package com.iiitb.scm.graphview.graphstructure;

/**
 * @author Harjinder
 */
public class Edge {
	private Attribute Attr = null;
	private Node nodeA = null;
	private Node nodeB = null;
	private int ID = -1;
	
	public Edge(){
		Attr = new Attribute();
		nodeA = null;
		nodeB = null;
	}
	
	public Edge(Node nodeA, Node nodeB, int ID){
		Attr = new Attribute();
		this.nodeA = nodeA;
		this.nodeB = nodeB;
		this.ID = ID;
		Attr.addAttribute("__GVName", "Edge_" + this.ID);
	}

	public Attribute getAttribute(){
		return this.Attr;
	}
	
	public void addAttribute(String strName, String strValue){
		this.Attr.addAttribute(strName, strValue);
	}
	
	public void appendAttributeWithoutOverWrite(Attribute attr){
		this.Attr.updateWithoutOverWrite(attr);
		//reset the original ID
		setID(getID());
	}
	
	public void appendAttributeWithOverWrite(Attribute attr){
		this.Attr.updateWithOverWrite(attr);
		//reset the original ID
		setID(getID());
	}
	
	public boolean isAround(int x, int y){
		//line: ax + by + c = 0 and m -> slope of the line
		double a=0, b=0, c=0, m=0, dist=0;
		int x1=0, y1=0, x2=0, y2=0;
		
		x1 = getX1();	y1 = getY1();
		x2 = getX2();	y2 = getY2();

		//if( (x==x1 && y==y1) || (x==x2&&y==y2) )
		//	return true;
		//else 
		
		if(x1!=x2){
			m = (double)(y2-y1)/(double)(x2-x1);
			a = m;
			b = -1;
			c = ((-1)*(m*x1)) + y1;

			dist = a*x + b*y + c;
			if(dist<0)
				dist*=(-1);
		
			dist = dist / Math.sqrt(a*a + b*b);
			if(dist<=20){
				if( (x>x1 && x<x2) || (x<x1 && x>x2) ){
					if( (y1==y2) || (y>y1 && y<y2) || (y<y1 && y>y2) )
						return true;
					else
						return false;
				}
				else
					return false;
			}
			else
				return false;
		}
		else{//Handle the verticle line...
			dist = Math.abs( (double)x - (double)x1);
			if(dist<=20){
				if( (y>y1 && y<y2) || (y<y1 && y>y2) )
					return true;
				else
					return false;
			}
			else
				return false;
		}
/*		if(y1 != y2){
			//for(int i=0; i<1; i++){
			//	x1 = x1 - i;	x2 = x2 - i;
			double diff = (double)(y2-y1)/(double)(x2-x1) - (double)(y2-y)/(double)(x2-x);
			if( diff >= (-0.1) && diff <= 0.1)
				return true;
			else
				return false;
			//}
			//for(int i=0; i<1; i++){
			//	x1 = x1 + i;	x2 = x2 + i;
			//	if((y2-y1)/(x2-x1) == (y2-y)/(x2-x))
			//		return true;			
			//}
		}
		else{
			for(int i=0; i<3; i++){
				y1 = y1 - i;	y2 = y2 - i;
				if((y2-y1)/(x2-x1) == (y2-y)/(x2-x))
					return true;			
			}
			for(int i=0; i<3; i++){
				y1 = y1 + i;	y2 = y2 + i;
				if((y2-y1)/(x2-x1) == (y2-y)/(x2-x))
					return true;			
			}
		}
*/
	}
	
	public int getX1(){
		return nodeA.getX();
	}
	public int getY1(){
		return nodeA.getY();
	}
	public int getX2(){
		return nodeB.getX();
	}
	public int getY2(){
		return nodeB.getY();
	}
	public void resetAttr(String strAll){
		this.Attr.reset(strAll);
	}
	
	void setID(int ID){
		this.ID = ID;
		Attr.remove("__GVName");
		Attr.addAttribute("__GVName", "Edge_" + this.ID);
	}
	public int getID(){
		return this.ID;
	}
	
	void delIncidence(){
		nodeA.delIncidentEdge(this);
		nodeB.delIncidentEdge(this);
	}

	public String getAttributeValue(String attrName){
		return this.Attr.getValue(attrName);
	}
	
	public Attribute getAttributes(){
		return this.Attr;
	}
	
	public void setAttributes(Attribute attr){
		this.Attr = attr;
	}
	
	public Node getNodeA(){
		return this.nodeA;
	}
	
	public Node getNodeB(){
		return this.nodeB;
	}
}
