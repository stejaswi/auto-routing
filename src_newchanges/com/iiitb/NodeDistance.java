package com.iiitb;

/**
 * FileName: NodeDistance.java
 * 
 * @author Sunil
 * @date of creation Jul 20, 2015
 * @Copyright: International Institute of Information Technology, Bangalore,
 *             India (IIITB). All rights Reserved.
 * @description The class finds the node distance
 * 
 */
public class NodeDistance {

	String startNodeName;
	String destNodeName;
	Double distance;

	public NodeDistance() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * NodeDistance constructor method
	 * 
	 * @param startNodeName
	 *            ,destNodeName and distance
	 */

	public NodeDistance(String startNodeName, String destNodeName,
			Double distance) {
		super();
		this.startNodeName = startNodeName;
		this.destNodeName = destNodeName;
		this.distance = distance;
	}

	// Getter and setters
	public String getStartNodeName() {
		return startNodeName;
	}

	public void setStartNodeName(String startNodeName) {
		this.startNodeName = startNodeName;
	}

	public String getDestNodeName() {
		return destNodeName;
	}

	public void setDestNodeName(String destNodeName) {
		this.destNodeName = destNodeName;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

}
