package com.iiitb;

import java.util.ArrayList;
import java.util.List;

/**
 * FileName: FindPossiblePath.java
 * 
 * @author Sunil
 * @date of creation Jul 20, 2015
 * @Copyright: International Institute of Information Technology, Bangalore,
 *             India (IIITB). All rights Reserved.
 * @description The class finds all possible paths of the graph
 * 
 */
public class FindPossiblePath {

	/**
	 * findPossiblePath method
	 * 
	 * @param nodeDistances
	 *            - list of node distances and startingNode (hub)
	 */

	public Double findPossiblePath(List<NodeDistance> nodeDistances,
			String startingNode) {
		String bestPath = new String();
		//Initial big number to capture bestPath value
		Double bestPathVal = 10000.0;
		Double ignoreLimit = 0.3;
		int originalPath = 1;
		Double firstPathDistance = 0.0;
		Double percentageDiff = 0.0;
		int ignoredNode = 0;
		// Get number of distinct nodes
		ArrayList<String> nodes = getNumberOfDistinctNodes(nodeDistances);

		//System.out.println("all nodes:" + nodes.toString());

		// Create two temporary arraylists to pass through the nodes
		ArrayList<String> tempNodes = new ArrayList<String>();
		ArrayList<String> tempNodes1 = new ArrayList<String>();
		for (String node : nodes) {
			tempNodes.add(node);
			tempNodes1.add(node);
		}
		tempNodes.remove(startingNode);
		tempNodes1.remove(startingNode);

		// Get possible combinations equal to n!
		int size = nodes.size() - 1;
		int possibilities = getPossibilities(size);

		System.out.println("Possibilities:" + possibilities);

		ArrayList<String> allPaths = new ArrayList<String>();

		// Get all paths - display possible node combinations
		allPaths = allAnagrams(tempNodes);
		//System.out.println(allPaths);

		// get corresponding edges for each path from hub to nodes and back to
		// hub
		String startNode = new String();
		String endNode = new String();
		for (String path : allPaths) {
			String pathTaken = new String();
			Double totalPath = 0.0;
			for (int i = 0; i < nodes.size(); i++) {
				if (i == 0) {
					startNode = startingNode;
					endNode = path.substring(i, i + 1);
				} else if (i == nodes.size() - 1) {
					endNode = startingNode;
					startNode = path.substring(i - 1, i);
				} else {
					startNode = path.substring(i - 1, i);
					endNode = path.substring(i, i + 1);
				}

				// Display the path taken
				//pathTaken = pathTaken + startNode + endNode + "==>";

				for (NodeDistance node : nodeDistances) {
					if (node.getStartNodeName().equals(startNode)
							&& node.getDestNodeName().equals(endNode)) {
						//Not counting the edges which are less than 0.3 
						//System.out.println("node distance:"+node.getDistance());
						if (originalPath==1){
							//System.out.println("ignored the node:"+startNode + endNode);
							totalPath = totalPath + node.getDistance();
							// Display the path taken
							pathTaken = pathTaken + startNode + endNode + "==>";
						}
						else if (node.getDistance() >= ignoreLimit){
							//System.out.println("node distance:"+node.getDistance());
						totalPath = totalPath + node.getDistance();
						// Display the path taken
						pathTaken = pathTaken + startNode + endNode + "==>";
						}
						//This condition to keep the original path intact
						

					}
				}
			}
			// Display path taken and total distance
			//if condition is to reduce the sysout size for n>8 as eclipse is crashing
			if (originalPath==1){
			System.out.println("Path Taken:" + pathTaken);
			System.out.println("Total Distance:" + totalPath);
			firstPathDistance = totalPath;
			originalPath = 0;
			}
			//Capturing the best path and store it for future reference
			if (totalPath < bestPathVal){
				bestPathVal = totalPath;
				bestPath = pathTaken;
			}
		}
		System.out.println("Best Path:" + bestPath);
		System.out.println("Best Distance:" + bestPathVal);
		System.out.println("Ignored nodes:"+ignoredNode);
		//Calculate the difference
		percentageDiff = ((firstPathDistance-bestPathVal)/firstPathDistance)*100;
		System.out.println("% reduction in Distance:" + percentageDiff + "%");
		
		return percentageDiff;

	}

	/**
	 * allAnagrams method
	 * 
	 * @param nodes
	 */

	public ArrayList<String> allAnagrams(ArrayList<String> nodes) {
		if (nodes.size() < 2) {
			return nodes;
		}
		ArrayList<String> allAnswers = new ArrayList<String>();
		for (int i = 0; i < nodes.size(); i++) {
			String node = nodes.get(i);
			ArrayList<String> tempNodes = new ArrayList<String>();
			for (String string : nodes) {
				tempNodes.add(string);
			}
			tempNodes.remove(node);
			ArrayList<String> shortwordArray = allAnagrams(tempNodes);
			for (int j = 0; j < shortwordArray.size(); j++) {
				allAnswers.add(node + shortwordArray.get(j));
			}
		}
		return allAnswers;
	}

	/**
	 * getPossibilities method n! ways of possible ways
	 * 
	 * @param n
	 *            -size
	 */

	public int getPossibilities(int size) {
		int possibilities = 1;
		for (int i = 1; i <= size; i++) {
			possibilities = possibilities * i;
		}
		return possibilities;
	}

	/**
	 * getNumberOfDistinctNodes method
	 * 
	 * @param nodeDistances
	 *            - list of node distances is input
	 */

	public ArrayList<String> getNumberOfDistinctNodes(
			List<NodeDistance> nodeDistances) {
		ArrayList<String> nodes = new ArrayList<String>();
		// Loop through all nodeDistances
		for (NodeDistance nodeDistance : nodeDistances) {
			if (!nodes.contains(nodeDistance.getStartNodeName())) {
				nodes.add(nodeDistance.getStartNodeName());
			}
			if (!nodes.contains(nodeDistance.getDestNodeName())) {
				nodes.add(nodeDistance.getDestNodeName());
			}
		}

		return nodes;
	}

	
}
