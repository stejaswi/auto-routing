package com.iiitb.scm.quadraticSolver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Vector;

public class CPLEXILPFile {

	
	public static void createLPFile(String objective, String objType,
		Vector<String> constraints, Vector<String> generalIntVariables, Vector<String> binaryVariables, File ILPFile) {
		BufferedWriter out = null;
		int constrNo = 0;
		try {
			out = new BufferedWriter(new FileWriter(ILPFile));
			out.write("Minimize");
			out.newLine();
			out.newLine();
			out.write(objective);
			out.newLine();
			out.newLine();
			out.write("Subject To");
			out.newLine();
			out.newLine();

			for (constrNo = 0; constrNo < constraints.size(); constrNo++) {
				out.write("C" + constrNo + ": " + constraints.get(constrNo));
				out.newLine();
			}
			out.newLine();
			if (binaryVariables != null) {
				out.write("Binary");
				out.newLine();
				for (constrNo = 0; constrNo < generalIntVariables.size(); constrNo++) {
					out.write(generalIntVariables.elementAt(constrNo));
					out.newLine();
				}
			}
			out.newLine();
			
			if (generalIntVariables.size() != 0) {
				out.write("General");
				out.newLine();
				for (constrNo = 0; constrNo < generalIntVariables.size(); constrNo++) {
					out.write(generalIntVariables.elementAt(constrNo));
					out.newLine();
				}
			}
			out.write("End");

			if (out != null) {
				out.flush();
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
