package com.iiitb;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class DataExtraction {

	List sheetData;
	List distanceList = null;
	List<NodeDistance> nodeDistances = null;
	List lats = null;
	List longs = null;
	static String previousSlotName;
	@SuppressWarnings("unchecked")
	public void readData() throws Exception {

		String filename = "data/t504.xls";
		sheetData = new ArrayList();
		FileInputStream fis = null;
		try {

			// Read the xls input file and add to the workbook of java
			fis = new FileInputStream(filename);
			HSSFWorkbook workbook = new HSSFWorkbook(fis);

			// Specify latlong sheet to be used for fetching the data
			HSSFSheet sheet = workbook.getSheetAt(1);
			Iterator rows = sheet.rowIterator();
			// Iterate over all rows and get data for required cell
			while (rows.hasNext()) {
				HSSFRow row = (HSSFRow) rows.next();
				Iterator cells = row.cellIterator();

				List data = new ArrayList();
				while (cells.hasNext()) {
					HSSFCell cell = (HSSFCell) cells.next();
					data.add(cell);
				}

				sheetData.add(data);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				fis.close();
			}
		}

		// showExelData(sheetData);
	}

	// Get the clusters and store them to vanSet
	public HashSet<String> getClusters() {
		HashSet<String> vansSet = new HashSet<String>();
		for (int i = 0; i < sheetData.size(); i++) {
			List list = (List) sheetData.get(i);
			HSSFCell vanNo = (HSSFCell) list.get(7);
			HSSFCell slotNo = (HSSFCell) list.get(6);

			// Prepare a set of unique vanNo and slotNo
			vansSet.add(vanNo.toString() + ":" + slotNo.toString());
		}
		return vansSet;
	}

	// Compute distances
	public String computeDistances(String filter, String slot,
			double hubLatitude, double hubLongitude, boolean isSource) {

		double arr[][] = new double[1000][2];
		int deliverySeqNumber =0;
		if(isSource)
		{
			arr[0][0] = hubLatitude;
			arr[0][1] = hubLongitude;
			deliverySeqNumber = 1;
			previousSlotName = slot;
		}
		
		double percentageDistReduction;
		// To calculate average percentage reduction
		String returnPerct = "N:0.0";

		for (int i = 0; i < sheetData.size(); i++) {
			List list = (List) sheetData.get(i);
			HSSFCell vanNo = (HSSFCell) list.get(7);
			vanNo.setCellType ( Cell.CELL_TYPE_STRING ) ;
			HSSFCell slotNo = (HSSFCell) list.get(6);
			slotNo.setCellType ( Cell.CELL_TYPE_STRING ) ;

			try {

				// for each cluster get lat and long details
				if (vanNo.getStringCellValue().equals(filter)
						&& slotNo.getStringCellValue().equals(slot)) {
					HSSFCell cell1 = (HSSFCell) list.get(2);
					cell1.setCellType(1);

					arr[deliverySeqNumber][0] = Double.parseDouble(cell1.getStringCellValue());
					HSSFCell cell2 = (HSSFCell) list.get(3);
					cell2.setCellType(1);
					arr[deliverySeqNumber][1] = Double.parseDouble(cell2.getStringCellValue());
					deliverySeqNumber++;

				}
			}// end of try
			catch (NumberFormatException e) {
				e.printStackTrace();
			}

		} // For loop
		
		if (!isSource){
			arr[deliverySeqNumber][0]= hubLatitude;
			arr[deliverySeqNumber][1] = hubLongitude;
			deliverySeqNumber++;
		}
		lats = new ArrayList();
		longs = new ArrayList();
		for (int i = 0; i < deliverySeqNumber; i++) {
			lats.add(arr[i][0]);
			longs.add(arr[i][1]);
			System.out.println("lat, long:" + arr[i][0] + " " + arr[i][1]);
		}
		// Initialize distanceList and create and instance of
		// GeodecDistance.java
		distanceList = new ArrayList<List>();
		GeodecDistance gd = new GeodecDistance();
		System.out.println("number of nodes:" + deliverySeqNumber);
		// Try for exhaustive search if number of nodes is less than or equal to
		// 10
		
		
		
			if(isSource){
				
				//Code for getting road distance using Google API
				List<String> originList = new ArrayList<String>();
				List<String> destinationList = new ArrayList<String>();
				
				for (int i = 0; i < deliverySeqNumber; i++) {
						for (int k = 1; k < deliverySeqNumber; k++) {
							originList.add( arr[i][0]+","+arr[i][1]);
							destinationList.add(arr[k][0]+","+arr[k][1]);
							
						}
				}
				
				ArrayList<ArrayList<String>> dis= new ArrayList<ArrayList<String>>();
				
				String[] origins = new String[originList.size()];
				origins = originList.toArray(origins);
				String[] destinations = new String[destinationList.size()];
				destinations = destinationList.toArray(destinations);
				ArrayList<ArrayList<String>> result = 
						gd.getGoogleAPIRoadDistance(originList,origins,destinationList,destinations,0,dis);				//End of Google API Usage
				
				System.out.println("******************************");
				System.out.println(result);
				System.out.println("******************************");

			
				for(int i=0;i<result.size();i++)
				{
					
					ArrayList<String> temp = result.get(i);
					//Very Ugly Edit - Prasanna Oct 19, 2015
					int n1 = temp.get(0).indexOf(",");
					int n2 = temp.get(1).indexOf(",");
					System.out.println("Origin: "+temp.get(0) + " Destination: "+temp.get(1)+ " Distance: "+temp.get(2) + 
							" GeodecDistance: " + 
							GeodecDistance.getGeodecDistance(
									Double.parseDouble(temp.get(0).substring(0, n1)),
									Double.parseDouble(temp.get(0).substring(n1+1,temp.get(0).length())),			
									Double.parseDouble(temp.get(1).substring(0, n2)),
									Double.parseDouble(temp.get(1).substring(n2+1,temp.get(1).length()))
									));
				}
				int count=0;
				double BBDistance=0.0;
				int r=-1;
				int s=0;
				
				for (int i = 0; i < deliverySeqNumber; i++) {
					// for (int k = i + 1; k < j; k++) {
					r=i;
					s=s+1;
					for (int k = 1; k < deliverySeqNumber; k++) {
						List nodeDistance = new ArrayList<List>();
						
							nodeDistance.add(slot+"_"+i);
							nodeDistance.add(slot+"_"+k);
						
						ArrayList<String> temp1 = result.get(count);
						String distance = temp1.get(2);
						int kmFlag=0;
						if(distance.contains("km"))
							kmFlag=1;
						else
							kmFlag=0;
						double dist = Double.parseDouble(distance.substring(0, distance.indexOf(' ')));
						if(kmFlag==0)
							dist=dist/1000;
						
						//if((Double.parseDouble(temp1.get(0).toString())==arr[i][0])&&(Double.parseDouble(temp1.get(1).toString())==arr[i][1]))
						if((i==r)&&(s==k))
						{
							
							BBDistance = BBDistance+dist;
							System.out.println("i:"+i+"r:+"+r+"s:"+s+"k:"+k+"BBDistance:"+BBDistance);
						}
						nodeDistance.add(dist);
						count++;
						distanceList.add(nodeDistance);
					}
				}
				System.out.println("BBDistance1:"+BBDistance);
		}//end of if(isSource)
		else
		{
			//Code for getting road distance using Google API
			List<String> originList = new ArrayList<String>();
			List<String> destinationList = new ArrayList<String>();
			
			for (int i = 0; i < deliverySeqNumber-1; i++) {
					for (int k = 0; k < deliverySeqNumber; k++) {
						originList.add( arr[i][0]+","+arr[i][1]);
						destinationList.add(arr[k][0]+","+arr[k][1]);
						
					}
			}
			
			ArrayList<ArrayList<String>> dis= new ArrayList<ArrayList<String>>();
			
			String[] origins = new String[originList.size()];
			origins = originList.toArray(origins);
			String[] destinations = new String[destinationList.size()];
			destinations = destinationList.toArray(destinations);
			ArrayList<ArrayList<String>> result = 
					gd.getGoogleAPIRoadDistance(originList,origins,destinationList,destinations,0,dis);//End of Google API Usage
			;
			//End of Google API Usage

			for(int i=0;i<result.size();i++)
			{
				
				ArrayList<String> temp = result.get(i);
				System.out.println("Origin: "+temp.get(0) + " Destination: "+temp.get(1)+ " Distance: "+temp.get(2));
			}
			
			//End of Google API call
			
			int resultCount=0;
			int r=-1;
			int s=0;
			double BBDistance=0;
			for (int i = 0; i < deliverySeqNumber-1; i++) {
				// for (int k = i + 1; k < j; k++) {
				r=i;
				s=i+1;
				for (int k = 0; k < deliverySeqNumber; k++) {
					List nodeDistance = new ArrayList<List>();
					
					if(k==deliverySeqNumber-1)
					{
						nodeDistance.add(slot+"_"+i);
						nodeDistance.add(previousSlotName+"_0");
					}
					else
					{
						nodeDistance.add(slot+"_"+i);
						nodeDistance.add(slot+"_"+k);
					}
					
					ArrayList<String> temp1 = result.get(resultCount);
					String distance = temp1.get(2);
					int kmFlag=0;
					if(distance.contains("km"))
						kmFlag=1;
					else
						kmFlag=0;
					double dist = Double.parseDouble(distance.substring(0, distance.indexOf(' ')));
					if(kmFlag==0)
						dist=dist/1000;
					if((i==r)&&(s==k))
					{
						
						BBDistance = BBDistance+ dist;
						System.out.println("i:"+i+"r:+"+r+"s:"+s+"k:"+k+"BBDistance:"+BBDistance);
					}
					nodeDistance.add(dist);
					distanceList.add(nodeDistance);
					resultCount++;
				}
			}
			
			System.out.println("BBDistance:"+BBDistance);
		}
			nodeDistances = new ArrayList<NodeDistance>();
			String source = "", destination = "";
			double dist = 0;

			for (int i = 0; i < distanceList.size(); i++) {
				List distance = (List) distanceList.get(i);
				for (int m = 0; m < distance.size(); m++)
					source = distance.get(0).toString();
				destination = distance.get(1).toString();
				dist = Double.valueOf(distance.get(2).toString());
				// Print to see all possible distances between source and
				// destination
				
				/*  System.out.println("source:" + source);
				 System.out.println("destination:" + destination);
				 System.out.println("dist:" + dist);*/
				 

				NodeDistance nodeDistanceSA = new NodeDistance(source,
						destination, dist);
				nodeDistances.add(nodeDistanceSA);

			
			/*System.out.println("Number of node distance combinations: "
					+ nodeDistances.size());
			// Call for all possible combinations
			FindPossiblePath findPossiblePath = new FindPossiblePath();
			// Starting point is "s"-hub
			percentageDistReduction = findPossiblePath.findPossiblePath(
					nodeDistances, "S");
			returnPerct = "Y:" + percentageDistReduction;*/
		}// if
		
		return returnPerct;
	}

	public List getLats() {
		return lats;
	}

	public List getLongs() {
		return longs;
	}

	public List<NodeDistance> getNodeDistances() {
		return nodeDistances;
	}

	public void setNodeDistances(List<NodeDistance> nodeDistances) {
		this.nodeDistances = nodeDistances;
	}

	public char nameMapping(int i) {
		String alphabet = "SABCDEFGHIJKLMNOPQRTUWXYZ";
		return alphabet.charAt(i);
	}

	public void extractData (String filter,String slot, double hubLatitude, double hubLongitude,boolean isSource ){
		HashSet<String> vanSet = new HashSet<String>();
		Double avgPerctReduction = 0.0;
		ArrayList<Double> allReductions = new ArrayList<Double>();
		Double reducedPerct = 0.0;
		String reducedPerctStr;
		String[] reducedPerctStrSplit = new String[2];
		try {
			readData();
			/*vanSet = getClusters();

			Iterator iterator = vanSet.iterator();
			while (iterator.hasNext()) {
				String clusters = iterator.next().toString();
				String[] clustersInfo = clusters.split(":");
				filter = clustersInfo[0];
				slot = clustersInfo[1];

				System.out.println("\nCluster combination: VAN: " + filter
						+ "slot " + slot);
				reducedPerctStr = computeDistances(filter, slot, 13.02886,
						77.5335698);*/
			computeDistances(filter, slot, hubLatitude,
					hubLongitude,isSource);
			
		}catch(Exception ae){
			ae.printStackTrace();
		}
	}
	
	
	public static void main(String args[]) {

		DataExtraction de = new DataExtraction();
		HashSet<String> vanSet = new HashSet<String>();
		String filter, slot;
		Double avgPerctReduction = 0.0;
		ArrayList<Double> allReductions = new ArrayList<Double>();
		Double reducedPerct = 0.0;
		String reducedPerctStr;
		String[] reducedPerctStrSplit = new String[2];
		try {
			de.readData();
			vanSet = de.getClusters();

			Iterator iterator = vanSet.iterator();
			while (iterator.hasNext()) {
				String clusters = iterator.next().toString();
				String[] clustersInfo = clusters.split(":");
				filter = clustersInfo[0];
				slot = clustersInfo[1];

				System.out.println("\nCluster combination: VAN: " + filter
						+ "slot " + slot);
				reducedPerctStr = de.computeDistances(filter, slot, 13.02886,
						77.5335698,true);
				// To calculate average reduction
				reducedPerctStrSplit = reducedPerctStr.split(":");
				if (reducedPerctStrSplit[0].equals("Y")) {
					allReductions.add(Double.valueOf(reducedPerctStrSplit[1]));
				}
			}
			// calculate average value of collected percentage reductions

			System.out.println("\n Summary of reduced percentages:");
			Double sum = 0.0;
			for (int i = 0; i < allReductions.size(); i++) {
				System.out.println("reduced percentages: " + allReductions.get(i));
				sum += allReductions.get(i);
			}
			avgPerctReduction = (sum / allReductions.size());
			System.out.println("Size of entries: " +  allReductions.size());
					
			System.out.println("Average Percentage of reduction: "
					+ avgPerctReduction);

		} catch (Exception ae) {
			ae.printStackTrace();
		}
	}

	public List getDistanceList() {
		return distanceList;
	}

	public void setDistanceList(List distanceList) {
		this.distanceList = distanceList;
	}

}
