package com.iiitb;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class DataExtraction {

	List sheetData;
	List distanceList = null;
	List<NodeDistance> nodeDistances = null;
	List lats = null;
	List longs = null;
	static String previousSlotName;
	static double firstSlotDistance = 0;
	static double secondSlotDistance = 0;
	
	@SuppressWarnings("unchecked")
	public void readData() throws Exception {

		String filename = "data/t504b.xls";
		sheetData = new ArrayList();
		FileInputStream fis = null;
		try {

			// Read the xls input file and add to the workbook of java
			fis = new FileInputStream(filename);
			HSSFWorkbook workbook = new HSSFWorkbook(fis);

			// Specify latlong sheet to be used for fetching the data
			HSSFSheet sheet = workbook.getSheetAt(1);
			Iterator rows = sheet.rowIterator();
			// Iterate over all rows and get data for required cell
			while (rows.hasNext()) {
				HSSFRow row = (HSSFRow) rows.next();
				Iterator cells = row.cellIterator();

				List data = new ArrayList();
				while (cells.hasNext()) {
					HSSFCell cell = (HSSFCell) cells.next();
					data.add(cell);
				}

				sheetData.add(data);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				fis.close();
			}
		}

		// showExelData(sheetData);
	}

	// Get the clusters and store them to vanSet
	public HashSet<String> getClusters() {
		HashSet<String> vansSet = new HashSet<String>();
		for (int i = 0; i < sheetData.size(); i++) {
			List list = (List) sheetData.get(i);
			HSSFCell vanNo = (HSSFCell) list.get(7);
			HSSFCell slotNo = (HSSFCell) list.get(6);

			// Prepare a set of unique vanNo and slotNo
			vansSet.add(vanNo.toString() + ":" + slotNo.toString());
		}
		return vansSet;
	}

	public static int BBFirstSlotDrops=0;
	public static int BBTotalDrops=0;
	public static double BBFirstSlotDistance=0;
	public static double BBFirstSlotTime=0;
	public static double BBjumpDistance=0;
	public static double BBTotalDistance=0;

	public static double lastSADropLat,lastSADropLong;
	
	// Compute distances
	public String computeDistances(String filter, String slot,
			double hubLatitude, double hubLongitude, boolean isSource) {

		double arr[][] = new double[1000][2];
		int deliverySeqNo =0;
		if(isSource)
		{
			arr[0][0] = hubLatitude;
			arr[0][1] = hubLongitude;
			deliverySeqNo = 1;
			previousSlotName = slot;
		}

		double percentageDistReduction;
		// To calculate average percentage reduction
		String returnPerct = "N:0.0";

		for (int i = 0; i < sheetData.size(); i++) {
			List list = (List) sheetData.get(i);
			HSSFCell vanNo = (HSSFCell) list.get(7);
			vanNo.setCellType ( Cell.CELL_TYPE_STRING ) ;
			HSSFCell slotNo = (HSSFCell) list.get(6);
			slotNo.setCellType ( Cell.CELL_TYPE_STRING ) ;

			try {

				// for each cluster get lat and long details
				if (vanNo.getStringCellValue().equals(filter)
						&& slotNo.getStringCellValue().equals(slot)) {
					HSSFCell cell1 = (HSSFCell) list.get(2);
					cell1.setCellType(1);

					arr[deliverySeqNo][0] = Double.parseDouble(cell1.getStringCellValue());
					HSSFCell cell2 = (HSSFCell) list.get(3);
					cell2.setCellType(1);
					arr[deliverySeqNo][1] = Double.parseDouble(cell2.getStringCellValue());
					deliverySeqNo++;

				}
			}// end of try
			catch (NumberFormatException e) {
				e.printStackTrace();
			}

		} // For loop
		
		//store the last drop in the first slot, to include this distance in BB Routing Tour Calculation
		if (isSource) {
			lastSADropLat=arr[deliverySeqNo-1][0];
			lastSADropLong=arr[deliverySeqNo-1][1];
			System.out.println("LL " + lastSADropLat + " LO" + lastSADropLong);
		}
 
		//if it is the 2nd slot, we return to the hub, and hence this setting - Prasanna October 25, 2015
		if (!isSource){
			arr[deliverySeqNo][0]= hubLatitude;
			arr[deliverySeqNo][1] = hubLongitude;
			deliverySeqNo++;
		}
		LogWriter.write("********************BB Routing Tour*************");
		lats = new ArrayList();
		longs = new ArrayList();
		for (int i = 0; i < deliverySeqNo; i++) {
			lats.add(arr[i][0]);
			longs.add(arr[i][1]);
			//System.out.println("lat, long:" + arr[i][0] + " " + arr[i][1]);
			LogWriter.write(arr[i][0] + "	" + arr[i][1]);

		}
		// Initialize distanceList and create and instance of
		// GeodecDistance.java
		distanceList = new ArrayList<List>();
		GeodecDistance gd = new GeodecDistance();
		//LogWriter.write("number of nodes:" + deliverySeqNo);

		if(isSource){

			//Code for getting road distance using Google API
			List<String> originList = new ArrayList<String>();
			List<String> destinationList = new ArrayList<String>();
			
			System.out.println("************ Total Drops " + deliverySeqNo + "************");
			for (int i = 0; i < deliverySeqNo; i++) 
				originList.add( arr[i][0]+","+arr[i][1]);
			for (int i = 0; i < deliverySeqNo; i++) 
				destinationList.add(arr[i][0]+","+arr[i][1]);//redundant, but keep the code the same
			
			ArrayList<ArrayList<String>> dis= new ArrayList<ArrayList<String>>();

			String[] origins = new String[originList.size()];
			origins = originList.toArray(origins);
			String[] destinations = new String[destinationList.size()];
			destinations = destinationList.toArray(destinations);
			
			//this automatically breaks up the call into small chunks
			ArrayList<ArrayList<String>> result = gd.getGoogleAPIRoadDistance(origins,destinations,0,dis);
			//End of Google API Usage

			for(int i=0;i<result.size();i++)
			{

				ArrayList<String> temp = result.get(i);
				String distance = temp.get(2);
				int kmFlag=0;
				if(distance.contains("km"))
					kmFlag=1;
				else
					kmFlag=0;
				double dist = Double.parseDouble(distance.substring(0, distance.indexOf(' ')));
				if(kmFlag==0)
					dist=dist/1000;
				//System.out.println("Origin: "+temp.get(0) + " Destination: "+temp.get(1)+ " Distance: "+temp.get(2) + " time: "+ (dist/20)*60 + " mins");

			}
			int count=0;
			double BBDistance=0.0;
			int r=-1;
			int s=0;
			// Doubly nested loop, both i, and k from 0 to (deliverySeqNo-1) - Prasanna Nov 15
			for (int i = 0; i < deliverySeqNo; i++) {
				// for (int k = i + 1; k < j; k++) {
				r=i;
				s=s+1;
				for (int k = 0; k < deliverySeqNo; k++) {
					List nodeDistance = new ArrayList<List>();

					nodeDistance.add(slot+"_"+i);
					nodeDistance.add(slot+"_"+k);

					ArrayList<String> temp1 = result.get(count);
					String distance = temp1.get(2);
					int kmFlag=0;
					if(distance.contains("km"))
						kmFlag=1;
					else
						kmFlag=0;
					double dist = Double.parseDouble(distance.substring(0, distance.indexOf(' ')));
					if(kmFlag==0)
						dist=dist/1000;

					//if((Double.parseDouble(temp1.get(0).toString())==arr[i][0])&&(Double.parseDouble(temp1.get(1).toString())==arr[i][1]))
					//if((i==r)&&(s==k))
					if (k == (i+1))
					{

						BBDistance = BBDistance+dist;
						System.out.println("i:"+i+"r:+"+r+"s:"+s+"k:"+k+"BBDistance:"+BBDistance);
					}
					nodeDistance.add(dist);
					count++;
					if (count == result.size())
						break;
					distanceList.add(nodeDistance);
				}
				if (count == result.size())
					break;
			}
			
			System.out.println("***********BBDistance Slot1*********");
			//assume vehicle speed of 20 Kmph, and 10 minutes per drop
			BBFirstSlotDrops=(deliverySeqNo-1);//don't count hub
			BBFirstSlotDistance=BBDistance;
			BBFirstSlotTime=BBDistance/20*60 + (deliverySeqNo-1)*10;//No time incorporated for hub
			System.out.println(
					"Slot 1: Total Drops: " + BBFirstSlotDrops 
					+ " BBDistance (Kms) "+ BBFirstSlotDistance 
					+ " Time (mins)" + BBFirstSlotTime);
			System.out.println("***********BBDistance Slot1*********");
			//Save it
			
		}//end of if(isSource)
		else
		{

			//Code for getting road distance using Google API
			List<String> originList = new ArrayList<String>();
			List<String> destinationList = new ArrayList<String>();
			
			System.out.println("************ Total Drops " + deliverySeqNo + "************");
			//Note, we have to return to hub, so we need the hub as the last destination, 
			//this is taken care of above by incrementing deliverySeqNo - Prasanna Nov 18, 2015
			for (int i = 0; i < (deliverySeqNo-1);i++)//one less origin
				originList.add( arr[i][0]+","+arr[i][1]);
			originList.add(lastSADropLat+ "," + lastSADropLong);//only to add this distance to BB Routing Distance
			for (int i = 0; i < deliverySeqNo; i++) 
				destinationList.add(arr[i][0]+","+arr[i][1]);//redundant, but keep the code the same
			
			ArrayList<ArrayList<String>> dis= new ArrayList<ArrayList<String>>();

			String[] origins = new String[originList.size()];
			origins = originList.toArray(origins);
			String[] destinations = new String[destinationList.size()];
			destinations = destinationList.toArray(destinations);
			ArrayList<ArrayList<String>> result = gd.getGoogleAPIRoadDistance(origins,destinations,0,dis);
			//End of Google API Usage

			for(int i=0;i<result.size();i++)
			{

				ArrayList<String> temp = result.get(i);
				String distance = temp.get(2);
				int kmFlag=0;
				if(distance.contains("km"))
					kmFlag=1;
				else
					kmFlag=0;
				double dist = Double.parseDouble(distance.substring(0, distance.indexOf(' ')));
				if(kmFlag==0)
					dist=dist/1000;
				System.out.println("Origin: "+temp.get(0) + " Destination: "+temp.get(1)+ " Distance: "+temp.get(2)+ " time: "+ (dist/20)*60 + " mins");
			}

			//End of Google API call

			int resultCount=0;
			int r=-1;
			int s=0;
			double BBDistance=0;
			for (int i = 0; i < deliverySeqNo-1; i++) {
				// for (int k = i + 1; k < j; k++) {
				r=i;
				s=i+1;
				for (int k = 0; k < deliverySeqNo; k++) {
					List nodeDistance = new ArrayList<List>();

					if(k==deliverySeqNo-1)
					{
						nodeDistance.add(slot+"_"+i);
						nodeDistance.add(previousSlotName+"_0");
					}
					else
					{
						nodeDistance.add(slot+"_"+i);
						nodeDistance.add(slot+"_"+k);
					}

					ArrayList<String> temp1 = result.get(resultCount);
					String distance = temp1.get(2);
					int kmFlag=0;
					if(distance.contains("km"))
						kmFlag=1;
					else
						kmFlag=0;
					double dist = Double.parseDouble(distance.substring(0, distance.indexOf(' ')));
					if(kmFlag==0)
						dist=dist/1000;
					//if((i==r)&&(s==k))
					if (k == (i+1))
					{

						BBDistance = BBDistance+ dist;
						System.out.println("i:"+i+"r:+"+r+"s:"+s+"k:"+k+"BBDistance:"+BBDistance);
					}
					nodeDistance.add(dist);
					distanceList.add(nodeDistance);
					resultCount++;
					if (resultCount == result.size())
					{
						//BBDistance = BBDistance+ dist;
						//System.out.println("i:"+i+"r:+"+r+"s:"+s+"k:"+k+"BBDistance:"+BBDistance);
						break;
					}
				}
				if (resultCount == result.size())
					break;
			}
			
			String distance = result.get((deliverySeqNo-1)*deliverySeqNo).get(2);
			BBjumpDistance = Double.parseDouble(distance.substring(0, distance.indexOf(' ')));


			System.out.println("");
			LogWriter.write("***********BBDistance Slot1*********");
			//assume vehicle speed of 20 Kmph, and 10 minutes per drop
			LogWriter.write(
					"Slot 1: Total Drops: " + BBFirstSlotDrops 
					+ " BBDistance (Kms) " + BBFirstSlotDistance 
					+ " Time (mins)" + BBFirstSlotTime);
			LogWriter.write("***********BB Jump Distance*********");
			LogWriter.write("Jumpover Distance (Kms) 	" + BBjumpDistance);
			LogWriter.write("***********BBDistance Slot2*********");
			//assume vehicle speed of 20 Kmph, and 10 minutes per drop
			LogWriter.write(
					"Slot 2: Total Drops: " + (deliverySeqNo-1) 
					+ " BBDistance (Kms) " + BBDistance 
					+ " Time (mins)" + (BBDistance/20*60 + (deliverySeqNo-1)*10));			
			LogWriter.write("");
			LogWriter.write("***********BBDistance Total*********");
			//assume vehicle speed of 20 Kmph, and 10 minutes per drop
			BBTotalDrops=(deliverySeqNo-1) + BBFirstSlotDrops;
			BBTotalDistance=BBDistance + BBFirstSlotDistance + BBjumpDistance;
			LogWriter.write(
					"Both Slots: Total Drops: " 
					+ ((deliverySeqNo-1) + BBFirstSlotDrops) 
					+ " BBDistance (Kms) " 
					+ (BBDistance + BBFirstSlotDistance + BBjumpDistance)
					+ " Time (mins)" 
					+ (BBDistance/20*60 + (deliverySeqNo-1)*10 + BBFirstSlotTime));
			LogWriter.write("");
			setFirstSlotDistance(BBFirstSlotDistance);
			setSecondSlotDistance(BBDistance);
		}
		nodeDistances = new ArrayList<NodeDistance>();
		String source = "", destination = "";
		double dist = 0;

		for (int i = 0; i < distanceList.size(); i++) {
			List distance = (List) distanceList.get(i);
			for (int m = 0; m < distance.size(); m++)
				source = distance.get(0).toString();
			destination = distance.get(1).toString();
			dist = Double.valueOf(distance.get(2).toString());
			// Print to see all possible distances between source and
			// destination

			/*  System.out.println("source:" + source);
				 System.out.println("destination:" + destination);
				 System.out.println("dist:" + dist);*/


			NodeDistance nodeDistanceSA = new NodeDistance(source,
					destination, dist);
			nodeDistances.add(nodeDistanceSA);


			/*System.out.println("Number of node distance combinations: "
					+ nodeDistances.size());
			// Call for all possible combinations
			FindPossiblePath findPossiblePath = new FindPossiblePath();
			// Starting point is "s"-hub
			percentageDistReduction = findPossiblePath.findPossiblePath(
					nodeDistances, "S");
			returnPerct = "Y:" + percentageDistReduction;*/
		}// if

		return returnPerct;
	}

	public List getLats() {
		return lats;
	}

	public List getLongs() {
		return longs;
	}

	public List<NodeDistance> getNodeDistances() {
		return nodeDistances;
	}

	public void setNodeDistances(List<NodeDistance> nodeDistances) {
		this.nodeDistances = nodeDistances;
	}

	public char nameMapping(int i) {
		String alphabet = "SABCDEFGHIJKLMNOPQRTUWXYZ";
		return alphabet.charAt(i);
	}

	public void extractData (String filter,String slot, double hubLatitude, double hubLongitude,boolean isSource ){
		HashSet<String> vanSet = new HashSet<String>();
		Double avgPerctReduction = 0.0;
		ArrayList<Double> allReductions = new ArrayList<Double>();
		Double reducedPerct = 0.0;
		String reducedPerctStr;
		String[] reducedPerctStrSplit = new String[2];
		try {
			readData();
			/*vanSet = getClusters();

			Iterator iterator = vanSet.iterator();
			while (iterator.hasNext()) {
				String clusters = iterator.next().toString();
				String[] clustersInfo = clusters.split(":");
				filter = clustersInfo[0];
				slot = clustersInfo[1];

				System.out.println("\nCluster combination: VAN: " + filter
						+ "slot " + slot);
				reducedPerctStr = computeDistances(filter, slot, 13.02886,
						77.5335698);*/
			computeDistances(filter, slot, hubLatitude,
					hubLongitude,isSource);

		}catch(Exception ae){
			ae.printStackTrace();
		}
	}


	public static void main(String args[]) {

		DataExtraction de = new DataExtraction();
		HashSet<String> vanSet = new HashSet<String>();
		String filter, slot;
		Double avgPerctReduction = 0.0;
		ArrayList<Double> allReductions = new ArrayList<Double>();
		Double reducedPerct = 0.0;
		String reducedPerctStr;
		String[] reducedPerctStrSplit = new String[2];
		try {
			de.readData();
			vanSet = de.getClusters();

			Iterator iterator = vanSet.iterator();
			while (iterator.hasNext()) {
				String clusters = iterator.next().toString();
				String[] clustersInfo = clusters.split(":");
				filter = clustersInfo[0];
				slot = clustersInfo[1];

				System.out.println("\nCluster combination: VAN: " + filter
						+ "slot " + slot);
				reducedPerctStr = de.computeDistances(filter, slot, 13.02886,
						77.5335698,true);
				// To calculate average reduction
				reducedPerctStrSplit = reducedPerctStr.split(":");
				if (reducedPerctStrSplit[0].equals("Y")) {
					allReductions.add(Double.valueOf(reducedPerctStrSplit[1]));
				}
			}
			// calculate average value of collected percentage reductions

			System.out.println("\n Summary of reduced percentages:");
			Double sum = 0.0;
			for (int i = 0; i < allReductions.size(); i++) {
				System.out.println("reduced percentages: " + allReductions.get(i));
				sum += allReductions.get(i);
			}
			avgPerctReduction = (sum / allReductions.size());
			System.out.println("Size of entries: " +  allReductions.size());

			System.out.println("Average Percentage of reduction: "
					+ avgPerctReduction);

		} catch (Exception ae) {
			ae.printStackTrace();
		}
	}

	public List getDistanceList() {
		return distanceList;
	}

	public void setDistanceList(List distanceList) {
		this.distanceList = distanceList;
	}

	public static double getSecondSlotDistance() {
		return secondSlotDistance;
	}

	public static void setSecondSlotDistance(double secondSlotDistance) {
		DataExtraction.secondSlotDistance = secondSlotDistance;
	}

	public static double getFirstSlotDistance() {
		return firstSlotDistance;
	}

	public static void setFirstSlotDistance(double firstSlotDistance) {
		DataExtraction.firstSlotDistance = firstSlotDistance;
	}

	
	

}
