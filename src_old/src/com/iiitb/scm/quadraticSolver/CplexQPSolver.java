/** 
 * CplexQPSolver: Interface to solve Quadratic Programming problems (QP, QCP) 
 *                through CPLEX (Code Base: CplexLPSolver)
 * @author Shreyas B.G.
 * @version 0.1
 * Creation Date 09-04-2007
 * Copyright IIITB
 */

/* History: 
 * ---------------------------------------------------------------------
 *      Date  |        Description of Change          |    Author      |
 * ---------------------------------------------------------------------
 *  09-04-07  |          Creation of File             |  Shreyas B.G.  |
 * ---------------------------------------------------------------------      
 */

package com.iiitb.scm.quadraticSolver;

// General Java Imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import java.io.*;

// CPLEX(ILOG) related imports
import ilog.concert.*;
import ilog.cplex.*;
@SuppressWarnings("unchecked")
public class CplexQPSolver {

	// Minimization or Maximization in CPLEX
	static public int MIN = 0, 
			MAX = 0; 

	static public int module;

	// Variable for creating Active Cplex model
	static protected IloCplex cplex;
	static protected IloCplex cplexCopy;
	static protected IloNumVar[] decisionVariables;
	public static String json="{\"node\":[";
	static boolean Wflag=false;
	static String prevNodeId="-1",prevProductId="-1",prevTime="-1";

	BufferedWriter output = null;
	OutputStream outputStrm = null;
	String filename;
	int iteration;
	static double objValue = 0.0;
	static String [][] solution = null;
	static String varFileName;

	public static IloCplex.Status status = null;

	/**
	 * CplexQPSolver constructor with default filename
	 * @param optData
	 * @param problemType
	 * @param optimizationType
	 * @throws SCMCplexException
	 */
	public CplexQPSolver(Vector<String> optData, int problemType,
			int optimizationType) throws SCMCplexException
			{
		try{
			cplex = null;
			cplex = new IloCplex();

			//Check if decision variables are correctly specified
			Iterator optDataItr = optData.iterator();
			while (optDataItr.hasNext())
			{
				String[] temp = optDataItr.next().toString().split("[+-]");
				for (int i = 0; i < temp.length; i++)
					if ((temp[i].contains("inv") == false) & 
							(temp[i].contains("flow") == false))
					{
						System.out.println("Decision variables absent in: \n" + 
								temp[i] + "\n");
						System.exit(1);
					}
			}

			String filename = "C:\\temp.lp";
			getDataIntoModel(optData, filename, problemType, optimizationType);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
			}

	/**
	 * CplexQPSolver constructor with input filename
	 * @param optData
	 * @param filename
	 * @param problemType
	 * @param optimizationType
	 * @throws SCMCplexException
	 */
	public CplexQPSolver(String filename,int itr) throws SCMCplexException
	{
		this.filename = filename;
		try{
			cplex = null;
			cplex = new IloCplex();
			getDataIntoModel(filename);
			this.filename = filename + Integer.toString(itr);
			this.iteration = itr;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}

	public CplexQPSolver()
	{

	}

	@SuppressWarnings("static-access")
	public void setOutputVarsFileName(String fileName)
	{
		this.varFileName = fileName;
	}
	/**
	 * cplexSolve() method to send problem to Cplex and receive soln
	 * @throws SCMCplexException
	 */

	public double cplexSolve() throws SCMCplexException
	{
		double result = 0;
		boolean flag=false;

		try {
			outputStrm = new FileOutputStream("objective solution.txt");
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		try
		{
			// inbuilt CPLEX method to solve active model
			try
			{
				cplex.setOut(new FileOutputStream("CPLEX_log.txt",true));
			}
			catch (FileNotFoundException f){};
			//cplex.setParam(IloCplex.IntParam.NodeAlg,IloCplex.Algorithm.Primal);
			//cplex.setParam(IloCplex.IntParam.RootAlg,IloCplex.Algorithm.Primal);
			//cplex.setParam(IloCplex.IntParam.ItLim, 1);
			//for(int i=0;i<1000;i++)

			flag = cplex.solve(); 

			try {
				output = new BufferedWriter(new FileWriter(varFileName));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}		

			// If solution exists, display status and results
			if( flag )				 
			{
				status = cplex.getStatus();
				System.out.println("CPLEX Status: " + status.toString());
				cplex.setOut(cplex.output());
				System.out.println("Solution value: " + cplex.getObjValue());
				result = cplex.getObjValue();
				objValue = result;
				cplexCopy=cplex;
				IloLPMatrix lp = (IloLPMatrix)cplex.LPMatrixIterator().next();
				decisionVariables = lp.getNumVars();
				double[] x = cplex.getValues(lp);
				solution= new String[x.length][2]; 
				for (int j = 0; j < x.length; ++j) {
					try {
						//System.out.println(decisionVariables[j].toString()+ ": " + x[j]);
						output.write(decisionVariables[j].toString() + ": " + x[j]);
						output.newLine();
						solution[j][0]= decisionVariables[j].toString();
						solution[j][1]= String.valueOf(x[j]);
						/////    Convert to JSON - START /////////
						String[] temp=solution[j][0].toString().split("_");  //// N_1_W_Inv_P_0_T_0  e.g. format 
						String productId  = temp[5];
 						 
						String nodeId=temp[1];
						//System.out.println(decisionVariables[j].toString()+ ": " + x[j]);
						if(temp[0].equals("N")&&temp[3].equals("Inv")){
							//System.out.print(solution[j][0]+":"+solution[j][1] );
							if(!prevNodeId.equals(nodeId)){
								if(!prevNodeId.equals("-1")){
									json = json.substring(0, json.length()-1); // remove , in the end
									json+="]}]},";
									prevProductId="-1";
								}
								else{
									
								}
								json+="{\"id\":"+temp[1]+",\"values\":[";

								prevNodeId=nodeId;


							}
						 if(prevNodeId.equals(nodeId)){
								if(!prevProductId.equals(productId)){
									if(!prevProductId.equals("-1")){
										json = json.substring(0, json.length()-1); 
										json+="]},";
									}
									json+="{\"label\":\"product-"+productId+"\",\"values\":["; 
									prevProductId=productId;
								}
								 if(prevProductId.equals(productId)){ 
									json+="{\"day\":"+temp[7]+",\"value\":"+solution[j][1]+"},"; 
									//prevProductId=productId;
								}
							}
						}

						/////// Convert to JSON - END /////////


					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//System.out.println(decisionVariables[j].toString() +  ": Value = " + x[j]);

				}
				try {
					output.close();
					json = json.substring(0, json.length()-1);

					json+="]}]}]}";
					System.out.print(json+" " );

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			// If solution does not exist display result status 
			else
			{
				status = cplex.getStatus();

				System.out.println( "Result Status : " + status.toString() );
			}

			// Release Cplex License after use
			cplex.end();
		}
		catch( IloException e )
		{
			System.out.println("Error in solving QP/QCP Model");
			e.printStackTrace();
		}
		try {
			outputStrm.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}// End of cplexSolve()	

	public IloCplex getCplexObject(){
		return cplexCopy;
	}
	/** 
	 * Put Input data in Vector to active Cplex model
	 * 
	 * @param optData
	 * @param problemType
	 * @param optimizationType
	 * @throws SCMCplexException
	 * @throws IloException
	 */
	static public void getDataIntoModel(Vector<String> optData,
			String filename,
			int problemType,
			int optimizationType) throws SCMCplexException,
			IloException
			{
		try{

			BufferedWriter out = new BufferedWriter(new FileWriter(filename));

			Iterator optDataItr = optData.iterator();
			ArrayList<String> constraints = new ArrayList<String>();
			ArrayList<String> bounds = new ArrayList<String>();
			String temp;
			if (optimizationType == MIN)
				out.write("Minimize" + "\n");
			else
				out.write("Maximize" + "\n");
			String temp0 = optDataItr.next().toString();
			out.append("obj: " + temp0 + "\n");
			out.append("Subject to " + "\n");
			while (optDataItr.hasNext())
			{
				temp = optDataItr.next().toString(); 
				if (temp.split("=").length == 2)
				{
					constraints.add(temp);
				}
				else if (temp.split("=").length == 3)
				{
					String[] temp2 = temp.split("=");
					String temp3 = temp2[1].toString();
					String[] temp4 = temp3.split("[+-]");
					if (temp4.length == 1)
					{
						bounds.add(temp);
					}

					else
						constraints.add(temp);
				}
			}			
			for (int i=0; i < constraints.size(); i++)
				out.append("c" + i + ": " + constraints.get(i) + "\n");
			out.append("Bounds" + "\n");
			for (int i = 0; i < bounds.size(); i++)
				out.append(bounds.get(i) + "\n");
			out.append("End");
			out.close();				

			cplex.importModel(filename);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

			}

	/**
	 * Put Input data from file to active Cplex model
	 * @param filename
	 * @throws SCMCplexException
	 * @throws IloException
	 */
	static public void getDataIntoModel(String filename) throws SCMCplexException,
	IloException
	{
		System.out.println("Importing model>>>>>>>>>>>>>>>>>>>>>>>>>>");
		cplex.importModel(filename);
		System.out.println("model imported");
	}


	/* SCMCplexException defined here for convenience purpose only, 
	 * this can be imported from the package in which it has been 
	 * declared (lpsolver)
	 */ 

	public class SCMCplexException extends IloException{
		private static final long serialVersionUID = 7526472295622776147L;

		//	Message initialised
		String message = null; 

		/**
		 * @param msg
		 */
		SCMCplexException( String msg )
		{
			// Calling super class constructor
			super(); 
			this.message = msg;
		}

		// Function showing the error message
		public String getMessage()
		{
			return message;
		}
	}

	public double getSolution()
	{
		return objValue;
	}
}
