/*
 * Created on Jul 14, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.iiitb.scm.graphview.graphstructure;
import java.util.Vector;

/**
 * @author Harjinder
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
@SuppressWarnings("unchecked")
public class Node {
	private Attribute Attr = null;
	private Vector vIncidentEdges = null;
	String nodeIdentifier = null;
	
	private int ID = -1, X=0, Y=0;
	
	public Node(){
		Attr = null;
		vIncidentEdges = null;
	}

	public Node(int x, int y, String strColor, int ID){
		Attr = new Attribute();
		this.X = x;
		this.Y = y;
		this.ID = ID;
		Attr.addAttribute("__GVName", "Node_" + this.ID);
		vIncidentEdges = new Vector();
	}

	public Node(int x, int y, int ID){
		Attr = new Attribute();
		this.X = x;
		this.Y = y;
		this.ID = ID;
		Attr.addAttribute("__GVName", "Node_" + this.ID);
		vIncidentEdges = new Vector();
	}
	
	

/*
	public Node(Attribute attr, int ID){
		//get a Copy of the Attribute instance...
		this.Attr = attr;
		this.ID = ID;
		Attr.addAttribute("__GVName", "Node_" + this.ID);
		vIncidentEdges = null;
	}
*/


	public Attribute getAttribute(){
		return this.Attr;
	}
	
	public void setAttributes(Attribute attr){
		this.Attr = attr;
	}
	
	public void addAttribute(String strName, String strValue){
		this.Attr.addAttribute(strName, strValue);
	}
	
	public void appendAttributeWithoutOverWrite(Attribute attr){
		this.Attr.updateWithoutOverWrite(attr);
		//reset the original ID
		setID(getID());
	}
	
	public void appendAttributeWithOverWrite(Attribute attr){
		this.Attr.updateWithOverWrite(attr);
		//reset the original ID
		setID(getID());
	}
	
	public boolean isAround(int x, int y){
		if((X-x)*(X-x) + (Y-y)*(Y-y) <= 100)
			return true;
		else
			return false;
	}
	
	public int getX(){
		return this.X;
	}

	public int getY(){
		return this.Y;
	}
	
	public void setX(int x){
		this.X = x;
	}

	public void setY(int y){
		this.Y = y;
	}

	public int getID(){
		return this.ID;
	}
	
	public void setID(int ID){
		this.ID = ID;
		Attr.remove("__GVName");
		Attr.addAttribute("__GVName", "Node_" + this.ID);
	}
	
	public void resetAttr(String strAll){
		this.Attr.reset(strAll);
	}

	public void addIncidentEdge(Edge e){
		this.vIncidentEdges.add(e);
	}
	
	public void delIncidentEdge(Edge e){
		Edge tmp = null;
		for(int i=0; i<vIncidentEdges.size(); i++){
			tmp = (Edge)vIncidentEdges.elementAt(i);
			if(e.getID() == tmp.getID()){
				vIncidentEdges.remove(e);
				return;
			}
		}
	}

	public Vector getIncidentEdges(){
		return this.vIncidentEdges;
	}

	public Vector getOriginatingEdges(){
		Vector result = new Vector();
		
		Edge e = null;
		Node n = null;
		
		for(int i=0; i<vIncidentEdges.size(); i++){
			e = (Edge)vIncidentEdges.elementAt(i);
			n = e.getNodeA();
			if(n.getID() == this.ID){
				result.add(e);
			}
		}
		return result;
	}
	
	public String getString(){
		String str = this.Attr.getString();
		str += " SIZE : " + vIncidentEdges.size() + " ";
		for(int i=0; i<vIncidentEdges.size(); i++){
			str += "EDGE : " + ((Edge)vIncidentEdges.elementAt(i)).getID();
		}
		return str;
	}
	
	public String getAttributeValue(String attrName){
		return this.Attr.getValue(attrName);
	}
	
	public Attribute getAttributes(){
		return this.Attr;
	}
	
	//This function will return all the neighbour nodes
	public Vector getAllNeighbours(){
		Vector result = new Vector();
		Vector incidentEdges = getIncidentEdges();
		Node n = null;

		if(incidentEdges!=null){
			for(int i=0; i<incidentEdges.size(); i++){
				n = ((Edge)incidentEdges.elementAt(i)).getNodeA();
				//check if the node is not me!!!
				if(n.getID() != this.getID()){
					result.addElement(n);
				}
				else{
					result.addElement(n);
				}
			}
		}
		return result;
	}

	//This function will return the nodes, to which we can reach from me!!!
	public Vector getReachableNodes(){
		Vector result = new Vector();
		Vector incidentEdges = getOriginatingEdges();
		Node n = null;

		if(incidentEdges!=null){
			for(int i=0; i<incidentEdges.size(); i++){
				n = ((Edge)incidentEdges.elementAt(i)).getNodeB();
				result.addElement(n);
			}
		}
		return result;
	}

	
}
