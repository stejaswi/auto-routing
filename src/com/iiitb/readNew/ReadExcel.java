package com.iiitb.readNew;


/*UncommentAfterIntegration-Keerthan
UncommentAfterIntegrationEnd-Keerthan*/

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import com.iiitb.LogWriter;
import com.iiitb.NodeDistance;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ReadExcel {
	static List<List<XSSFCell>> sheetData;
	static List<List<List<XSSFCell>>> sheets;	
	static String previousSlotName;
	static double firstSlotDistance = 0;
	static double secondSlotDistance = 0;
	List<NodeDistance> nodeDistances = null;
	List distanceList = null;
	List <Double> lats = null;
	List <Double> longs = null;
	HashMap<String, String> addressToLatLongMap = null;
	HashMap<String,String> orderToOrderDistanceMap = null;
	HashMap<String,String> hubOrderDistanceMap = null;

	public ReadExcel(){
		sheets = new ArrayList<List<List<XSSFCell>>>();
	}

	public void readDataOld() throws Exception {
		try {
			sheetData = new ArrayList<List<XSSFCell>>();
			String filename = "data/TestData_15FEB2016.xlsx";
			File myFile = new File(filename);
			FileInputStream fis = new FileInputStream(myFile);
			XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);
			XSSFSheet mySheet = myWorkBook.getSheetAt(0);

			Iterator<Row> rowIterator = mySheet.rowIterator();
			while (rowIterator.hasNext()) {
				XSSFRow row = (XSSFRow) rowIterator.next();

				Iterator<Cell> cellIterator = row.cellIterator();
				List<XSSFCell> data = new ArrayList<XSSFCell>();
				while (cellIterator.hasNext()) {
					XSSFCell cell = (XSSFCell) cellIterator.next();
					data.add(cell);
				}
				sheetData.add(data);
			}
			myWorkBook.close();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
	}

	public void readData() throws Exception {
		try {

			String filename = "data/TestData_15FEB2016.xlsx";
			File myFile = new File(filename);
			FileInputStream fis = new FileInputStream(myFile);
			XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);
			for(int i = 0 ; i < myWorkBook.getNumberOfSheets() ; i++)
			{	
				List<List<XSSFCell>> SheetData = new ArrayList<List<XSSFCell>>();
				XSSFSheet mySheet = myWorkBook.getSheetAt(i);
				Iterator<Row> rowIterator = mySheet.rowIterator();
				while (rowIterator.hasNext()) {
					XSSFRow row = (XSSFRow) rowIterator.next();

					Iterator<Cell> cellIterator = row.cellIterator();
					List<XSSFCell> data = new ArrayList<XSSFCell>();
					while (cellIterator.hasNext()) {
						XSSFCell cell = (XSSFCell) cellIterator.next();
						data.add(cell);
					}
					SheetData.add(data);				
				}

				sheets.add(SheetData);
			}
			myWorkBook.close();
			addressToLatLongMap = getAddresses();
			orderToOrderDistanceMap = getOrderToOrderDistance();
			hubOrderDistanceMap = getHubToOrderDistance();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
	}

	public HashSet<String> getClusters() {
		HashSet<String> vansSet = new HashSet<String>();
		for (int i = 1; i < (sheets.get(0)).size(); i++) { 		//van:slots clusters are in 0th sheet
			List <XSSFCell> tempList = (sheets.get(0)).get(i);	
			XSSFCell vanNo = tempList.get(4);					//van is 4th feature in 0th sheet
			vanNo.setCellType(Cell.CELL_TYPE_STRING);
			XSSFCell slotNo = tempList.get(3);					//slots are 4th feature in 0th sheet
			slotNo.setCellType(Cell.CELL_TYPE_STRING);
			vansSet.add(vanNo.toString() + ":" + slotNo.toString());			
		}
		return vansSet;
	}

	public HashSet<String> getVans() {
		HashSet<String> vansSet = new HashSet<String>();
		for (int i = 1; i < (sheets.get(0)).size(); i++) { 		//van:slots clusters are in 0th sheet
			List <XSSFCell> tempList = (sheets.get(0)).get(i);	
			XSSFCell vanNo = tempList.get(4);					//van is 4th feature in 0th sheet
			vanNo.setCellType(Cell.CELL_TYPE_STRING);
			vansSet.add(vanNo.toString() );			
		}
		return vansSet;
	}




	public HashMap<String, String> getAddresses() {
		HashMap<String, String> addressToLatLongMAp = new HashMap<String, String>();
		for (int i = 1; i < (sheets.get(0)).size(); i++) { 		//addressId:coordinates clusters are in 0th sheet
			List <XSSFCell> tempList = (sheets.get(0)).get(i);	
			XSSFCell addressId = tempList.get(0);					
			addressId.setCellType(Cell.CELL_TYPE_STRING);
			XSSFCell latLong = tempList.get(2);					
			latLong.setCellType(Cell.CELL_TYPE_STRING);
			addressToLatLongMAp.put( latLong.toString(), addressId.toString() );
		}
		return addressToLatLongMAp;
	}

	/*Keerthan March 27, 2016 :
	 * In sheet no. 1 - "IJ links", distance(order1 to order2) != distance(order2 to order1)
	 * Also, if there are 'n' unique orders there are n*n entries in the table 
	 * ^ verified this by reading the rows in Hashset and its counts.
	 * So the map is with key = "FROM_ORDER_ID:TO_ORDER_ID" and value = "distance"
	 */
	public HashMap<String,String> getOrderToOrderDistance() {
		HashMap<String,String> orderToOrderDistanceMap = new HashMap<String,String>();
		for (int i = 1; i < (sheets.get(1)).size(); i++) { 		//	:FROM ADDRESS ID(i):TO ADDRESS ID(j):DISTANCE(dij) are in 1th sheet
			List <XSSFCell> tempList = (sheets.get(1)).get(i);	
			XSSFCell fromAddress = tempList.get(0);					
			fromAddress.setCellType(Cell.CELL_TYPE_STRING);
			XSSFCell toAddress = tempList.get(1);					
			toAddress.setCellType(Cell.CELL_TYPE_STRING);
			XSSFCell dist = tempList.get(2);					
			dist.setCellType(Cell.CELL_TYPE_STRING);

			//if(fromAddress.equals("783025_7") && toAddress.equals("7036524_6"))
			//System.out.println();

			orderToOrderDistanceMap.put( (fromAddress.toString()+":"+toAddress.toString()) , dist.toString());

		}
		return orderToOrderDistanceMap;
	}

	//In sheet no. - 2 "Hub to order links", let us assume that hub to order and order to hub distances are same.
	//So the map is with key = ORDER_ID and value = distance
	public HashMap<String,String> getHubToOrderDistance() {
		HashMap<String,String> HubOrderDistanceMap = new HashMap<String,String>();
		for (int i = 1; i < (sheets.get(2)).size(); i++) { 		
			List <XSSFCell> tempList = (sheets.get(2)).get(i);	
			XSSFCell orderID = tempList.get(1);					
			orderID.setCellType(Cell.CELL_TYPE_STRING);
			XSSFCell dist = tempList.get(2);					
			dist.setCellType(Cell.CELL_TYPE_STRING);
			HubOrderDistanceMap.put( orderID.toString() , dist.toString());

		}
		return HubOrderDistanceMap;
	}


	public HashSet<String> getClustersOld() {
		HashSet<String> vansSet = new HashSet<String>();
		for (int i = 0; i < sheets.size(); i++) {
			List <XSSFCell> tempList = sheetData.get(i);
			XSSFCell vanNo = (XSSFCell)tempList.get(4);
			XSSFCell slotNo = (XSSFCell)tempList.get(3);
			vansSet.add(vanNo.toString() + ":" + slotNo.toString());
		}
		return vansSet;
	}	

	public static int BBFirstSlotDrops=0;
	public static int BBTotalDrops=0;
	public static double BBFirstSlotDistance=0;
	public static double BBFirstSlotTime=0;
	public static double BBjumpDistance=0;
	public static double BBTotalDistance=0;

	public static double lastSADropLat,lastSADropLong;

	public String computeDistances(String filter, String slot, double hubLatitude, double hubLongitude, boolean isSource) {
		double arr[][] = new double[1000][2];
		int deliverySeqNo = 0;
		if(isSource)
		{
			arr[0][0] = hubLatitude;
			arr[0][1] = hubLongitude;
			deliverySeqNo = 1;
			previousSlotName = slot;
		}
		double percentageDistReduction;
		// To calculate average percentage reduction
		String returnPerct = "N:0.0";

		for (int i = 1; i < (sheets.get(0)).size(); i++) {		//Skip first line
			List<XSSFCell> list = (sheets.get(0)).get(i);
			XSSFCell vanNo = (XSSFCell) list.get(4);
			vanNo.setCellType(Cell.CELL_TYPE_STRING) ;
			XSSFCell slotNo = (XSSFCell) list.get(3);
			slotNo.setCellType(Cell.CELL_TYPE_STRING) ;

			try {
				if (vanNo.getStringCellValue().equals(filter) && slotNo.getStringCellValue().equals(slot)) {
					String tempCoord = list.get(2).toString();
					List<String> coordx = Arrays.asList(tempCoord.split(","));
					String cell1 = coordx.get(0);
					String cell2 = coordx.get(1);
					arr[deliverySeqNo][0] = Double.parseDouble(cell1);
					arr[deliverySeqNo][1] = Double.parseDouble(cell2);
					deliverySeqNo++;
				}
			} // end of try block
			catch (NumberFormatException e) {
				e.printStackTrace();
			}

		}// end of for loop 

		//store the last drop in the first slot, to include this distance in BB Routing Tour Calculation
		if (isSource) {
			lastSADropLat=arr[deliverySeqNo-1][0];
			lastSADropLong=arr[deliverySeqNo-1][1];
			System.out.println("LL " + lastSADropLat + " LO" + lastSADropLong);
		}

		//if it is the 2nd slot, we return to the hub, and hence this setting - Prasanna October 25, 2015
		if (!isSource){
			arr[deliverySeqNo][0]= hubLatitude;
			arr[deliverySeqNo][1] = hubLongitude;
			deliverySeqNo++;
		}
		LogWriter.write("********************BB Routing Tour*************");
		lats = new ArrayList<Double>();
		longs = new ArrayList<Double>();
		for (int i = 0; i < deliverySeqNo; i++) {
			lats.add(arr[i][0]);
			longs.add(arr[i][1]);
			//System.out.println("lat, long:" + arr[i][0] + " " + arr[i][1]);
			LogWriter.write(arr[i][0] + "	" + arr[i][1]);

		}

		// Initialize distanceList and create and instance of
		// GeodecDistance.java
		distanceList = new ArrayList<List>();
		//GeodecDistance gd = new GeodecDistance();

		if(isSource){

			//Code for getting road distance using Google API
			List<String> originList = new ArrayList<String>();
			List<String> destinationList = new ArrayList<String>();

			System.out.println("************ Total Drops " + deliverySeqNo + "************");
			for (int i = 0; i < deliverySeqNo; i++) 
				originList.add( arr[i][0]+","+arr[i][1]);
			for (int i = 0; i < deliverySeqNo; i++) 
				destinationList.add(arr[i][0]+","+arr[i][1]);//redundant, but keep the code the same


			String[] origins = new String[originList.size()];
			origins = originList.toArray(origins);
			String[] destinations = new String[destinationList.size()];
			destinations = destinationList.toArray(destinations);

			ArrayList<ArrayList<String>> result = getRoadDistanceMatrix(origins,destinations,0);

			int count=0;
			double BBDistance=0.0;
			int r=-1;
			int s=0;
			// Doubly nested loop, both i, and k from 0 to (deliverySeqNo-1) - Prasanna Nov 15
			for (int i = 0; i < deliverySeqNo; i++) {
				// for (int k = i + 1; k < j; k++) {
				r=i;
				s=s+1;
				for (int k = 0; k < deliverySeqNo; k++) {
					List nodeDistance = new ArrayList<List>();

					nodeDistance.add(slot+"_"+i);
					nodeDistance.add(slot+"_"+k);

					ArrayList<String> temp1 = result.get(count);
					String distance = temp1.get(2);
					double dist = Double.valueOf(distance);

					if (k == (i+1))
					{

						BBDistance = BBDistance+dist;
						System.out.println("i:"+i+"r:+"+r+"s:"+s+"k:"+k+"BBDistance:"+BBDistance);
					}
					nodeDistance.add(dist);
					count++;
					if (count == result.size())
						break;
					distanceList.add(nodeDistance);
				}
				if (count == result.size())
					break;
			}

			System.out.println("***********BBDistance Slot1*********");
			//assume vehicle speed of 20 Kmph, and 10 minutes per drop
			BBFirstSlotDrops=(deliverySeqNo-1);//don't count hub
			BBFirstSlotDistance=BBDistance;
			BBFirstSlotTime=BBDistance/20*60 + (deliverySeqNo-1)*10;//No time incorporated for hub
			System.out.println(
					"Slot 1: Total Drops: " + BBFirstSlotDrops 
					+ " BBDistance (Kms) "+ BBFirstSlotDistance 
					+ " Time (mins)" + BBFirstSlotTime);
			System.out.println("***********BBDistance Slot1*********");
			//Save it

		}//end of if(isSource)
		else
		{

			List<String> originList = new ArrayList<String>();
			List<String> destinationList = new ArrayList<String>();

			System.out.println("************ Total Drops " + deliverySeqNo + "************");
			//Note, we have to return to hub, so we need the hub as the last destination, 
			//this is taken care of above by incrementing deliverySeqNo - Prasanna Nov 18, 2015
			for (int i = 0; i < (deliverySeqNo-1);i++)//one less origin
				originList.add( arr[i][0]+","+arr[i][1]);
			originList.add(lastSADropLat+ "," + lastSADropLong);//only to add this distance to BB Routing Distance
			for (int i = 0; i < deliverySeqNo; i++) 
				destinationList.add(arr[i][0]+","+arr[i][1]);//redundant, but keep the code the same

			String[] origins = new String[originList.size()];
			origins = originList.toArray(origins);
			String[] destinations = new String[destinationList.size()];
			destinations = destinationList.toArray(destinations);

			ArrayList<ArrayList<String>> result = getRoadDistanceMatrix(origins,destinations,0);

			int resultCount=0;
			int r=-1;
			int s=0;
			double BBDistance=0;
			for (int i = 0; i < deliverySeqNo-1; i++) {

				r=i;
				s=i+1;
				for (int k = 0; k < deliverySeqNo; k++) {
					List nodeDistance = new ArrayList<List>();

					if(k==deliverySeqNo-1)
					{
						nodeDistance.add(slot+"_"+i);
						nodeDistance.add(previousSlotName+"_0");
					}
					else
					{
						nodeDistance.add(slot+"_"+i);
						nodeDistance.add(slot+"_"+k);
					}

					ArrayList<String> temp1 = result.get(resultCount);
					String distance = temp1.get(2);
					double dist = Double.valueOf(distance);
					if (k == (i+1))
					{

						BBDistance = BBDistance+ dist;
						System.out.println("i:"+i+"r:+"+r+"s:"+s+"k:"+k+"BBDistance:"+BBDistance);
					}
					nodeDistance.add(dist);
					distanceList.add(nodeDistance);
					resultCount++;
					if (resultCount == result.size())
					{
						//BBDistance = BBDistance+ dist;
						//System.out.println("i:"+i+"r:+"+r+"s:"+s+"k:"+k+"BBDistance:"+BBDistance);
						break;
					}
				}
				if (resultCount == result.size())
					break;
			}

			for (int k = 0; k < deliverySeqNo; k++) {
				List nodeDistance = new ArrayList<List>();
				nodeDistance.add(previousSlotName+"_0");
				if(k!=deliverySeqNo-1){
					nodeDistance.add(slot+"_"+k);
				}
				else {
					nodeDistance.add(previousSlotName+"_0");
				}
				ArrayList<String> temp1 = result.get(resultCount);
				String distance = temp1.get(2);
				double dist = Double.valueOf(distance);

				nodeDistance.add(dist);
				distanceList.add(nodeDistance);
				resultCount++;
				if (resultCount == result.size())
				{
					//BBDistance = BBDistance+ dist;
					//System.out.println("i:"+i+"r:+"+r+"s:"+s+"k:"+k+"BBDistance:"+BBDistance);
					break;
				}
			}

			String distance = result.get((deliverySeqNo-1)*deliverySeqNo).get(2);
			BBjumpDistance = Double.valueOf(distance);


			System.out.println("");
			LogWriter.write("***********BBDistance Slot1*********");
			//assume vehicle speed of 20 Kmph, and 10 minutes per drop
			LogWriter.write(
					"Slot 1: Total Drops: " + BBFirstSlotDrops 
					+ " BBDistance (Kms) " + BBFirstSlotDistance 
					+ " Time (mins)" + BBFirstSlotTime);
			LogWriter.write("***********BB Jump Distance*********");
			LogWriter.write("Jumpover Distance (Kms) 	" + BBjumpDistance);
			LogWriter.write("***********BBDistance Slot2*********");
			//assume vehicle speed of 20 Kmph, and 10 minutes per drop
			LogWriter.write(
					"Slot 2: Total Drops: " + (deliverySeqNo-1) 
					+ " BBDistance (Kms) " + BBDistance 
					+ " Time (mins)" + (BBDistance/20*60 + (deliverySeqNo-1)*10));			
			LogWriter.write("");
			LogWriter.write("***********BBDistance Total*********");
			//assume vehicle speed of 20 Kmph, and 10 minutes per drop
			BBTotalDrops=(deliverySeqNo-1) + BBFirstSlotDrops;
			BBTotalDistance=BBDistance + BBFirstSlotDistance + BBjumpDistance;
			LogWriter.write(
					"Both Slots: Total Drops: " 
							+ ((deliverySeqNo-1) + BBFirstSlotDrops) 
							+ " BBDistance (Kms) " 
							+ (BBDistance + BBFirstSlotDistance + BBjumpDistance)
							+ " Time (mins)" 
							+ (BBDistance/20*60 + (deliverySeqNo-1)*10 + BBFirstSlotTime));
			LogWriter.write("");
			setFirstSlotDistance(BBFirstSlotDistance);
			setSecondSlotDistance(BBDistance);
		}
		//UncommentAfterIntegration-Keerthan
		nodeDistances = new ArrayList<NodeDistance>();
		//UncommentAfterIntegrationEnd-Keerthan
		String source = "", destination = "";
		double dist = 0;

		for (int i = 0; i < distanceList.size(); i++) {
			List distance = (List) distanceList.get(i);
			for (int m = 0; m < distance.size(); m++)
				source = distance.get(0).toString();
			destination = distance.get(1).toString();
			dist = Double.valueOf(distance.get(2).toString());
			// Print to see all possible distances between source and
			// destination

			/*  System.out.println("source:" + source);
				 System.out.println("destination:" + destination);
				 System.out.println("dist:" + dist);*/

			//UncommentAfterIntegration-Keerthan
			NodeDistance nodeDistanceSA = new NodeDistance(source,
					destination, dist);
			nodeDistances.add(nodeDistanceSA);
			//UncommentAfterIntegrationEnd-Keerthan

			/*System.out.println("Number of node distance combinations: "
					+ nodeDistances.size());
			// Call for all possible combinations
			FindPossiblePath findPossiblePath = new FindPossiblePath();
			// Starting point is "s"-hub
			percentageDistReduction = findPossiblePath.findPossiblePath(
					nodeDistances, "S");
			returnPerct = "Y:" + percentageDistReduction;*/
		}// if

		return returnPerct;
	}	

	public List getLats() {
		return lats;
	}

	public List getLongs() {
		return longs;
	}

	public List<NodeDistance> getNodeDistances() {
		return nodeDistances;
	}

	public void setNodeDistances(List<NodeDistance> nodeDistances) {
		this.nodeDistances = nodeDistances;
	}

	public char nameMapping(int i) {
		String alphabet = "SABCDEFGHIJKLMNOPQRTUWXYZ";
		return alphabet.charAt(i);
	}

	public void extractData (String filter,String slot, double hubLatitude, double hubLongitude,boolean isSource ){
		HashSet<String> vanSet = new HashSet<String>();
		Double avgPerctReduction = 0.0;
		ArrayList<Double> allReductions = new ArrayList<Double>();
		Double reducedPerct = 0.0;
		String reducedPerctStr;
		String[] reducedPerctStrSplit = new String[2];
		try {
			readData();
			/*vanSet = getClusters();

			Iterator iterator = vanSet.iterator();
			while (iterator.hasNext()) {
				String clusters = iterator.next().toString();
				String[] clustersInfo = clusters.split(":");
				filter = clustersInfo[0];
				slot = clustersInfo[1];

				System.out.println("\nCluster combination: VAN: " + filter
						+ "slot " + slot);
				reducedPerctStr = computeDistances(filter, slot, 13.02886,
						77.5335698);*/
			computeDistances(filter, slot, hubLatitude,
					hubLongitude,isSource);

		}catch(Exception ae){
			ae.printStackTrace();
		}
	}




	public ArrayList<ArrayList<String>> getRoadDistanceMatrix(String[] origins, String[] destinations,int index){
		ArrayList<ArrayList<String>> dis= new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> unChecked;

		//This function gives results in a 1-D array [source-lat,source-long dest-lat,dest-long, distance]
		for(int i=0;i < origins.length;i++) {
			ArrayList<ArrayList<String>> temp;
			String originOrderId = new String();
			String destOrderId = new String();

			//String[] tempOrigins = new String[1];
			//tempOrigins[0]= origins[i];
			for(int j=0;j<destinations.length;j++)
			{

				//if(origins[i].equals("13.027151417584,77.5767656432794") && destinations[j].equals("13.0278186892678,77.5713908915405"))
				//System.out.println();

				originOrderId = addressToLatLongMap.get( origins[i]);
				destOrderId = addressToLatLongMap.get( destinations[j]);

				String distance = new String();						
				if(StringUtils.isBlank(originOrderId)){ 
					if(StringUtils.isBlank(destOrderId)) //if both origin and dest order id are null
						distance = "0";
					else
						distance = hubOrderDistanceMap.get(destOrderId); //if origin order id is null only
				}
				else if(StringUtils.isBlank(destOrderId)) //only dest id is null					
					distance = hubOrderDistanceMap.get(originOrderId); //if null then assumed from hub
				else if(originOrderId.equals(destOrderId)) //both order id are same
					distance = "0";
				else{ //proper case
					distance = orderToOrderDistanceMap.get(originOrderId+":"+destOrderId);	
					if(StringUtils.isBlank(distance))
						distance = orderToOrderDistanceMap.get(destOrderId+":"+originOrderId);	
				}
				ArrayList<String> t = new ArrayList<String>();
				t.add(origins[i]);
				t.add(destinations[j]);
				t.add(distance);
				dis.add(t);
			}
		}

		unChecked=dis;
		//just prints getGeodecDistance. 
		/*System.out.println(unChecked);		

		int nPairs= unChecked.size();
		for(int count=0;count < nPairs;count++) {
			double lat1,lon1,lat2,lon2;
			ArrayList<String> temp1 = unChecked.get(count);

			String or=temp1.get(0);
			int N = or.indexOf(',');
			lat1 = Double.parseDouble(or.substring(0,N));
			lon1 = Double.parseDouble(or.substring(N+1, or.length()));

			String de=temp1.get(1);
			int M = de.indexOf(',');
			lat2 = Double.parseDouble(de.substring(0,M));
			lon2 = Double.parseDouble(de.substring(M+1, de.length()));


			String distance = temp1.get(2);
			double dist = Double.valueOf(distance);
			double gd = getGeodecDistance(lat1, lon1, lat2, lon2);
			System.out.println("Geo: " + gd + " Routed (km) " + dist);

			if (gd > dist)//generally minor errors
				System.out.println("----------------------Geo: " + gd + "Routed: " + dist);

		}*/
		return(unChecked);//now checked
	}



	/**
	 * Method to Calculate the geodec distances between
	 * two latitudes and longitudes.
	 */	
	public double getGeodecDistance(double lat1, double long1, double lat2, double long2){
		System.out.println("Inside geodec distance calculation");
		double earthRadius = 6371000; // Radius in meters
		double latitude = Math.toRadians(lat2-lat1);
		double longitude = Math.toRadians(long2-long1);
		double a = Math.sin(latitude/2) * Math.sin(latitude/2) +
				Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
				Math.sin(longitude/2) * Math.sin(longitude/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = (double) ((earthRadius * c)/1000);
		return distance;
		//return( Math.round(distance*100)/100.0d);
	}	


	public List getDistanceList() {
		return distanceList;
	}


	public void setDistanceList(List distanceList) {
		this.distanceList = distanceList;
	}


	public static double getSecondSlotDistance() {
		return secondSlotDistance;
	}


	public static void setSecondSlotDistance(double secondSlotDistance) {
		//UncommentAfterIntegration-Keerthan
		ReadExcel.secondSlotDistance = secondSlotDistance;
		//UncommentAfterIntegrationEnd-Keerthan*/
	}


	public static double getFirstSlotDistance() {
		return firstSlotDistance;
	}

	public static void setFirstSlotDistance(double firstSlotDistance) {
		//UncommentAfterIntegration-Keerthan
		ReadExcel.firstSlotDistance = firstSlotDistance;
		//UncommentAfterIntegrationEnd-Keerthan*/
	}





	public static void main(String args[]) throws Exception {
		ReadExcel de = new ReadExcel();
		/*
		 * 		de.readData();
		String a = ((sheets.get(0).get(1)).get(2)).toString();  //(1,2) of 1st sheet
		System.out.println(a);
		String b = ((sheets.get(1).get(1)).get(2)).toString(); //(1,2) of 2nd sheet
		System.out.println(b);
		String c = ((sheets.get(2).get(1)).get(2)).toString(); //(1,2) of 3rd sheet
		System.out.println(c);

		List<String> coordx = Arrays.asList(a.split(","));
		Double my = Double.parseDouble(coordx.get(1));
		System.out.println(my.getClass().getName());
		System.out.println(my);
		 */

		HashSet<String> vanSet = new HashSet<String>();
		String filter, slot;
		Double avgPerctReduction = 0.0;
		ArrayList<Double> allReductions = new ArrayList<Double>();
		Double reducedPerct = 0.0;
		String reducedPerctStr;
		String[] reducedPerctStrSplit = new String[2];
		try {
			de.readData();
			vanSet = de.getClusters();
			List<String> list = new ArrayList<String>(vanSet);
			Collections.sort(list);
			System.out.println(list);

			//			Iterator iterator = vanSet.iterator();
			//			while (iterator.hasNext()) {
			//				String clusters = iterator.next().toString();
			//				String[] clustersInfo = clusters.split(":");
			//				filter = clustersInfo[0];
			//				slot = clustersInfo[1];
			//
			//				System.out.println("\nCluster combination: VAN: " + filter
			//						+ "slot " + slot);
			//				reducedPerctStr = de.computeDistances(filter, slot, 13.02886,
			//						77.5335698,true);
			//				// To calculate average reduction
			//				reducedPerctStrSplit = reducedPerctStr.split(":");
			//				if (reducedPerctStrSplit[0].equals("Y")) {
			//					allReductions.add(Double.valueOf(reducedPerctStrSplit[1]));
			//				}
			//			}
			//			// calculate average value of collected percentage reductions
			//
			//			System.out.println("\n Summary of reduced percentages:");
			//			Double sum = 0.0;
			//			for (int i = 0; i < allReductions.size(); i++) {
			//				System.out.println("reduced percentages: " + allReductions.get(i));
			//				sum += allReductions.get(i);
			//			}
			//			avgPerctReduction = (sum / allReductions.size());
			//			System.out.println("Size of entries: " +  allReductions.size());
			//
			//			System.out.println("Average Percentage of reduction: "
			//					+ avgPerctReduction);

		} catch (Exception ae) {
			ae.printStackTrace();
		}




	}

}
