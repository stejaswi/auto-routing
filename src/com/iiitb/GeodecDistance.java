package com.iiitb;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import com.iitb.tspILProuting.TSPILPRun;

public class GeodecDistance {
	
	GeodecDistance obj = null;
	DistanceMatrix matrix;
	public GeodecDistance(){
		
		
	}
	
	public GeodecDistance getInstance(){
		if(this.obj==null)
			obj = new GeodecDistance();
			
		return this.obj;
	}
	
	/**
	 * Method to Calculate the geodec distances between
	 * two latitudes and longitudes.
	 */
	
	public double getGeodecDistance(double lat1, double long1, double lat2, double long2){
		System.out.println("Inside geodec distance calculation");
		double earthRadius = 6371000; // Radius in meters
	    double latitude = Math.toRadians(lat2-lat1);
	    double longitude = Math.toRadians(long2-long1);
	    double a = Math.sin(latitude/2) * Math.sin(latitude/2) +
	               Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
	               Math.sin(longitude/2) * Math.sin(longitude/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double distance = (double) ((earthRadius * c)/1000);
	    return distance;
	   //return( Math.round(distance*100)/100.0d);
	    }
	
		
	//just gets the routed distances, and ensure that we are (much) more than geodesic
	//this is essential to avoid getting meaningless results! - Prasanna October 25, 2015
	//index, dis are dummy arguments just to keep old calls working
	public ArrayList<ArrayList<String>> getGoogleAPIRoadDistance(String[] origins, String[] destinations,int index,ArrayList<ArrayList<String>> dis){
		ArrayList<ArrayList<String>> unChecked;
		
		/*System.out.println("***********----------------------**************");
		System.out.println(origins.length);
		System.out.println(destinations.length);
		System.out.println("***********----------------------**************");
		for(String or:origins)
			System.out.println(or);
		System.out.println("***********----------------------**************");
		for(String de:destinations)
			System.out.println(de);
		System.out.println("***********----------------------**************");*/
		
		
		//throw it away, it is just to keep old calls working - we can deprecate with another method, but code cleaning is for later.
		dis= null;

		//for simplicity, just do it row by row
		for(int i=0;i < origins.length;i++) {
			ArrayList<ArrayList<String>> temp;
			String[] tempOrigins = new String[1];
			tempOrigins[0]= origins[i];
			temp=getGoogleAPIRoadDistance1(tempOrigins,destinations);
			if (dis != null) dis.addAll(temp);
			else
				dis=temp;
		}
		
		unChecked=dis;
		
		System.out.println(unChecked);
		
		//This function gives results in a 1-D array [source-lat,source-long dest-lat,dest-long, distance]
		//Duplicate pairs are eliminated - Prasanna Oct 25, 2015
		//this API eliminates duplicate pairs, so we cannot use origins/destinations directly
		
		int nPairs= unChecked.size();
		for(int count=0;count < nPairs;count++) {
			double lat1,lon1,lat2,lon2;
			ArrayList<String> temp1 = unChecked.get(count);

			String or=temp1.get(0);
			int N = or.indexOf(',');
			lat1 = Double.parseDouble(or.substring(0,N));
			lon1 = Double.parseDouble(or.substring(N+1, or.length()));

			String de=temp1.get(1);
			int M = de.indexOf(',');
			lat2 = Double.parseDouble(de.substring(0,M));
			lon2 = Double.parseDouble(de.substring(M+1, de.length()));
			
			String distance = temp1.get(2);
			double dist = Double.parseDouble(distance.substring(0, distance.indexOf(' ')));
			if(!distance.contains("km"))//answer in meters
				dist=dist/1000;
			double gd = getGeodecDistance(lat1, lon1, lat2, lon2);
			System.out.println("Geo: " + gd + " Routed (km) " + dist);

			if (gd > dist)//generally minor errors
				System.out.println("----------------------Geo: " + gd + "Routed: " + dist);
				
		}
		return(unChecked);//now checked
	}

	public ArrayList<ArrayList<String>> getGoogleAPIRoadDistance1(String[] origins, String[] destinations) {
		ArrayList<ArrayList<String>> dis = new ArrayList<ArrayList<String>>();

		//cycle the keys to avoid loading just one
		String[] keyList = {
				"AIzaSyBjNHEmr7YCrWO_l_yuPMu9eJIJSFeXCa8",
				"AIzaSyCAYx1F8UtCIrVsR-CEkwe2xf3GueDFtis",
				"AIzaSyD-7Prua09B4-xnzKqA_lUjLfpqyCF-TGI",
				"AIzaSyCC11mTyxRdyVcX6K5IPm7wrbO_FOWmCcc",
				"AIzaSyBITksYO60Fld9ea_dr3N1s0SaAWfeTnUc",
				"AIzaSyBK1-wlbFpvjj1efG4Nzv1b9gjAMo-4B2c",
				"AIzaSyCdYmbfcqjPLkkpmuI9VmQAgEv-WOreURo",
				"AIzaSyBN3hBLe_3eCMLkwiWF1LgWU5hxdX2aWHE",				
				"AIzaSyCSs6LnB3pgZ-WxyJxWIv-rMBNrW8AVeUs",
				"AIzaSyBFMjH0Up_4HV9jIFSwUNYwIItFOcg9RHE"
		};
    	GeoApiContext context = new GeoApiContext().setApiKey(keyList[TSPILPRun.clusterCount % keyList.length]);

		//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBITksYO60Fld9ea_dr3N1s0SaAWfeTnUc");
		//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyCAYx1F8UtCIrVsR-CEkwe2xf3GueDFtis");
    	//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBK1-wlbFpvjj1efG4Nzv1b9gjAMo-4B2c");
    	//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBFMjH0Up_4HV9jIFSwUNYwIItFOcg9RHE");
    	//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyCC11mTyxRdyVcX6K5IPm7wrbO_FOWmCcc");

    	try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	try {
    		
    		matrix = DistanceMatrixApi.getDistanceMatrix(context, origins, destinations).await();
    		System.out.println("Matrix Rows: " + matrix.rows.length + " Matrix Columns " + matrix.rows[0].elements.length);
    		
    		for(int i=0;i<origins.length;i++)
    			for(int j=0;j<destinations.length;j++)
    			{
    				String distance = matrix.rows[i].elements[j].distance.toString();
    				ArrayList<String> temp = new ArrayList<String>();
    				temp.add(origins[i]);
    				temp.add(destinations[j]);
    				temp.add(distance);
    				dis.add(temp);
    			}
    	}
    	catch (Exception e){
        	try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        	e.printStackTrace();
        }
		return dis;
	}

	public ArrayList<ArrayList<String>> getGoogleAPIRoadDistance2(String[] origins, String[] destinations,int index,ArrayList<ArrayList<String>> dis){
		 //Double dist = 0.0;
		 int i=0;
		 boolean flag = false;
		 try {
				 String [] smallerOrigins = new String[2];
				 String [] smallerDestinations = new String[2];
				 
				 
				 int j=0;
				 for(i=index;i<(index+2) && i<origins.length;i++)
			 	 {
					flag = true;
			 		smallerOrigins[j]= origins[i];
			 		smallerDestinations[j]=destinations[i];
			 		j++;
			 		//System.out.println("Source: "+origins[i] + " Destination: "+destinations[i]);
			 	 }
				 
			 	
			 	//System.out.println(origins.length + " "+destinations.length);
	            if(flag)
	            {
	            	
	            	GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBITksYO60Fld9ea_dr3N1s0SaAWfeTnUc");
	            	//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyCAYx1F8UtCIrVsR-CEkwe2xf3GueDFtis");
	            	//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBK1-wlbFpvjj1efG4Nzv1b9gjAMo-4B2c");
	            	try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
	            	matrix = DistanceMatrixApi.getDistanceMatrix(context, smallerOrigins, smallerDestinations).await();
	            	
	            	int count = 0;
	            	//System.out.println("Reached here");
	            	for(int k=0;k<j;k++)
	            	{
	            		
	            		String distance = matrix.rows[count].elements[count].distance.toString();
	            		ArrayList<String> temp = new ArrayList<String>();
	            		temp.add(smallerOrigins[k]);
	            		temp.add(smallerDestinations[k]);
	            		temp.add(distance);
	            		
	            		dis.add(temp);
	            		
	            		//System.out.println("Source: "+smallerOrigins[k] + " Destination: "+smallerDestinations[k] + " Distance: " + distance);
	            		count++;
	            	}
	            	getGoogleAPIRoadDistance2(origins,destinations,i,dis);
	            } 
	            
	        }
	        catch (Exception e){
	        	try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	        	e.printStackTrace();
	        	//getGoogleAPIRoadDistance(origins,destinations,i,dis);
	        	
	        	//System.out.println("Exception in Google API usage:");
	        	//e.printStackTrace();
	        }
		 
		 return dis;
	}

	public double getGoogleAPIDistanceValues(int k){
		
		System.out.println("Distance using Google API: "+matrix.rows[k].elements[0].distance);
        String distance = matrix.rows[k].elements[0].distance.toString();
        distance = distance.substring(0, distance.indexOf(' '));
        return(Double.parseDouble(distance));
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		GeodecDistance gd = new GeodecDistance();
		double distance = gd.getGeodecDistance(13.02886, 77.53356980000001, 13.05081134, 77.59931527999993);
		System.out.println(distance);
		
	}



	

}
