package com.iitb.tspILProuting;

import java.util.Vector;

public interface ILPProblem {

	public String createObjectiveFunction();
	public Vector createConstraints();
	public Vector createBounds();
	public Vector createILPVariables();
	public Vector createBinaryVariables();
	
}
