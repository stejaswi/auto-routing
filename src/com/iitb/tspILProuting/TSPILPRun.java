package com.iitb.tspILProuting;

import java.io.*;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.iiitb.DataExtraction;
import com.iiitb.readNew.ReadExcel;
import com.iiitb.LogWriter;
import com.iiitb.scm.graphview.graphstructure.Edge;
import com.iiitb.scm.quadraticSolver.CPLEXILPFile;
import com.iiitb.tspILProuting.config.Config;
import com.sun.org.apache.xml.internal.security.keys.content.KeyInfoContent;

import sun.rmi.runtime.Log;


public class TSPILPRun {

	public static int clusterCount=0;
	public static double clusterPerDelta=0;
	public static double avgPerDelta=0;

	public static void main(String args[]) throws IOException{

		TSPILPRun obj = new TSPILPRun();
		//String [] list = {"1531YP(22)","1622YP(10)","1732YP(29)","1756YP(21)","1770YP(28)","2306YP(31)","2491YP(1)","456YP(5)","7060YP(16)","7485YP(4)","7525YP(18)"};
		/*String [] list = {
				//"51 BD MH-14-EM-3409 BNR",
				//"52 BD MH-14-EM-2433 BNR",
				//"53 BD MH-14-EM-4249 BNR",
				//"54 BD MH-14-EM-4766 BNR",
				//"55 BD MH-14-EM-3411 BNR",
				//"56 PD MH-14-EM-4045 BNR",
				"698", "185",
				"57 PD MH-14-EM-3668 BNR","58 PD MH-14-EM-3669 BNR","59 PD MH-14-EM-2778 BNR","60 PD MH-14-EM-1554 BNR",
				"61 PD MH-14-EM-3109 BNR","62 PD MH-14-EM-4188 BNR","63 PD MH-14-EM-0180 BNR","64 PD MH-14-EM-4274 BNR","65 PD MH-14-EM-3162 BNR",
				"66 RD MH-14-EM-2060 BNR","67 RD MH-12-LT-3759 BNR","68 RD MH-12-LT-2159 BNR","69 RD MH-12-LT-2185 BNR","70 RD MH-12-LT-2219 BNR",
				"71 RD MH-14-EM-1319 BNR","72 RD MH-12-LT-1660 BNR","73 RD MH-12-LT-1850 BNR","74 RD MH-12-LT-1949 BNR",
				"75 RD MH-12-LT-2708 BNR","76 SD MH-43-AD-9521 BNR","77 SD MH-43-AD-9523 BNR","78 SD MH-43-AD-9524 BNR",
				"79 SD MH-43-AD-9525 BNR","80 SD MH-43-AD-9526 BNR","81 SD MH-43-AD-9318 BNR","82 SD MH-43-AD-9319 BNR",
				"83 SD MH-43-AD-9320 BNR","84 SD MH-43-AD-9321 BNR","85 SD MH-43-AD-9104 BNR","86 SD MH-43-AD-9105 BNR",
				"87 SD MH-43-AD-9106 BNR","88 SD MH-43-AD-9107 BNR","89 SD MH-43-BB-62 BNR","90 SD MH-43-BB-63 BNR",
				"91 SD MH-43-BB-59 BNR","92 SD MH-43-BB-60 BNR","93 SD MH-43-AD-9860 BNR","94 SD MH-43-AD-9861 BNR",
				"95 SD MH-43-BB-57 BNR","96 SD MH-43-BB-58 BNR","97 SD MH-43-AD-9862 BNR","98 BD MH-14-EM-3936 BNR",
				"100 RD 3 BNR","116 XV 1 BNR","123 XV 8 BNR","124 XV 9 BNR","128 XV 13 BNR",
				"129 XV 14 BNR","130 XV 15 BNR",};
				clusterCount=list.length; */
		ReadExcel de = new ReadExcel();
		try {
			de.readData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HashSet<String> vanSet = new HashSet<String>();
		vanSet = de.getVans();
		List<String> list = new ArrayList<String>(vanSet);
		//List<String> list = new ArrayList<String>();
		//list.add("698");
		//list.add("185");
		//list.add("516");

		
		clusterCount=list.size();
		Config.loadConfig();

		String finalStats=System.getProperty("user.dir")+"/Output/stats.txt";
		FileOutputStream statFile = new FileOutputStream(new File(finalStats));
		Writer sFw = new BufferedWriter(new OutputStreamWriter(statFile));

		sFw.write("-----------------------------------------");
		sFw.write("Performance Improvement Statistics");
		sFw.write("\n");
		sFw.write("-----------------------------------------");
		sFw.write("\n");
		for(int i=0;i < list.size();i++)  {
			System.out.println("Loop No "+i+" Van_Id "+list.get(i)+" Size "+list.size());
			obj.runBBTSP(list.get(i),System.getProperty("user.dir")+"/Output/log"+list.get(i) + ".txt");
			sFw.write("Van: " + list.get(i) + " : " + ((int) (10*clusterPerDelta))/10.0+ "%");
			sFw.write("\n");
			avgPerDelta=(avgPerDelta*i+clusterPerDelta)/(i+1);
		}
		sFw.write("\n");
		sFw.write("-----------------------------------------");
		sFw.write("\n");
		sFw.write("Overall Percentage improvement: " + ((int) (10*avgPerDelta)/10.0 + "%"));
		sFw.close();
	}

	/**
	 * @param args
	 */
	public void runBBTSP(String cluster,String logFileName) {
		// TODO Auto-generated method stub

		TSPILPDataPreprocessing runObject = new TSPILPDataPreprocessing();
		LogWriter.initializeLog(new File(logFileName));
		LogWriter.write("Starting Logger...");
		runObject.dataExtraction(cluster, "S3",18.618364635652,73.761006945759,true);
		//runObject.dataExtraction("1531YP(22)", "S1",13.02886,
		//77.5335698,true);
		runObject.dataExtraction(cluster, "S4",18.618364635652,73.761006945759,false);
		//runObject.dataExtraction("1531YP(22)","S2",13.02886,
		//77.5335698,false);
		Vector nodes = runObject.getNodes();
		Vector edges = runObject.getEdges();
		TSPILPConstraints tc = new TSPILPConstraints();
		tc.populateInputNetwork(nodes,edges,runObject.numNodesNW1,runObject.numNodesNW2);
		tc.createILPModel();
		//Create CPLEX file to solve ILP
		//We need to add a constraint that every slot has to be finished in 3 hours
		CPLEXILPFile.createLPFile(tc.getObjective(), tc.getObjectiveType(), tc.getConstraints(), 
				tc.getILPVariables(), null, new File(System.getProperty("user.dir")+"/Output/BBRouting.lp"));
		/*LogWriter.write("****************** LAT/LONGS ****************************");
		for(int i=0;i<edges.size();i++) {
			Edge e= (Edge) edges.get(i);
			LogWriter.write(e.getID() + " " + e.getAttributeValue("name") + " " + 
					e.getNodeA().getAttributeValue("latitude") + " " + 
					e.getNodeA().getAttributeValue("longitude"));
		}*/

		try
		{

			System.out.println("Solver Path:"+Config.SOLVER_PATH);
			ProcessBuilder pb =
					new ProcessBuilder(Config.SOLVER_PATH,"ResultFile=out.sol", "BBRouting.lp");
			Map<String, String> env = pb.environment();
			//env.put("GRB_LICENSE_FILE","/opt/gurobi650/linux64/gurobi.lic");
			//pb.directory(new File("/home/savs95/workspace_adv_algo/BigBasketRouting/Output"));
			pb.directory(new File(System.getProperty("user.dir") + "/Output"));
			//pb.redirectErrorStream(true);
			//pb.redirectOutput(Redirect.appendTo(log));
			Process p = pb.start();
			//assert pb.redirectInput() == Redirect.PIPE;
			//assert pb.redirectOutput().file() == log;
			//assert p.getInputStream().read() == -1;
			p.waitFor();

			BufferedReader reader=new BufferedReader(
					new FileReader(System.getProperty("user.dir") + "/Output/out.sol"));
					//new FileReader("/home/savs95/workspace_adv_algo/BigBasketRouting/Output/out.sol"));

			LogWriter.write("*****************Objective function and Variable Values***************");
			String line;
			String[][] vars = new String[1000][4];
			int varLen=0;
			while((line = reader.readLine()) != null) {
				if (line.contains("Objective") ||
						line.contains("duction")){ //Tour Length Reduction 
					LogWriter.write(line);//dump the objective
					if (line.contains("Objective")) 
						clusterPerDelta=(1-Double.parseDouble(line.split("=")[1])/ReadExcel.BBTotalDistance)*100;
				}
				if (line.contains(" 1") & (!line.contains("u"))) { //can do much simpler 
					String sS, sD, n, m;
					String[] ls = line.split("_");
					sS=ls[0];
					sD=ls[2];
					n=ls[1];
					m=ls[3].split(" ")[0];
					System.out.println(sS + " " + n +  " " + sD + " " + m);
					vars[varLen][0]=sS;
					vars[varLen][1]=n;
					vars[varLen][2]=sD;
					vars[varLen++][3]=m;
				}
			}
			reader.close();


			//now traverse the tour
			//error handling has to be done - Prasanna Nov 12, 2015
			LogWriter.write("*******************New Tour*********************");
			int curPos=0;
			String curSource, curDest, curEdgeName;
			//Only if this does not appear, is the processing ok
			String hubLatLong = "**************Error******************";
			for(int i=0;i<varLen;i++){
				String[] temp= vars[curPos];
				curSource = temp[0] + "_" + temp[1];
				curDest = temp[2] + "_" + temp[3];
				curEdgeName = curSource + "_" + curDest;

				//print the current source's lat/long? Iterate over edges to find this - a hash map will be better
				//but this seems a quick fix, since I don't understand the code fully - Prasanna Nov 12, 2015
				for(int j=0;j<edges.size();j++) {
					Edge e= (Edge) edges.get(j);
					if (e.getAttributeValue("name").equals(curEdgeName)) {
						//this is the correct edge
						double lA,loA;
						//source is NodeA of the edge (edge is from e.getNodeA() to e.getNodeB()
						lA= Double.parseDouble(e.getNodeA().getAttributeValue("latitude"));
						loA= Double.parseDouble(e.getNodeA().getAttributeValue("longitude"));

						LogWriter.write(//e.getID() + " " + e.getAttributeValue("name") + " " + 
								lA + "	" + loA);//Node A of the current edge is shared with the previous edge
						if (i ==0)
							hubLatLong=lA + "	" + loA;//store it to write it again at the end
						break;
					}
				}
				//next node, which is the destination of the flow above
				//iterate over the vars array instead of the edges array - ugly - Prasanna Nov 12, 2015
				for(int j=0;j<vars.length;j++) {
					String[] tempVar=vars[j];
					String tempSource = tempVar[0] + "_" + tempVar[1];
					if (tempSource.equals(curDest)) {
						curPos=j;
						break;
					}
				}				
			}
			//print the return to the hub at the end
			LogWriter.write(hubLatLong);
			LogWriter.write("*******************New Tour Ends*********************");
			LogWriter.write("Percentage Distance Reduction: " + (int) clusterPerDelta);
			LogWriter.close();
		}
		catch(IOException e1) {
			e1.printStackTrace();
		}
		catch(InterruptedException e2) {
			e2.printStackTrace();
		}
		//execute Gurobi from shell
		//read output
		//check that the answer is a tour
		//print lat longs for both the old tour and this optimized tour
	}


	/*	public static void main(String[] args) {
		// TODO Auto-generated method stub


		for( int k=0;k <1; k++)
		{
			TSPILPDataPreprocessing runObject = new TSPILPDataPreprocessing();
			runObject.dataExtraction("1531YP(22)", "S1",13.02886,
					77.5335698,true,"S2_"+k);
			runObject.dataExtraction("1531YP(22)", "S2",13.02886,
					77.5335698,false,"S2_"+k);
			Vector nodes = runObject.getNodes();
			Vector edges = runObject.getEdges();
			TSPILPConstraints tc = new TSPILPConstraints();
			tc.populateInputNetwork(nodes,edges,runObject.numNodesNW1,runObject.numNodesNW2,"S2_"+k);
			tc.createILPModel();
			//Create CPLEX file to solve ILP
			CPLEXILPFile.createLPFile(tc.getObjective(), tc.getObjectiveType(), tc.getConstraints(), 
					tc.getILPVariables(), null, new File(System.getProperty("user.dir")+"\\output\\BBRouting_"+k+".lp"));

		}

	}*/

}
