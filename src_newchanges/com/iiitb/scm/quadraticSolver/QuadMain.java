/** 
 * QuadMain: Testing CplexQPSolver
 * @author Shreyas B.G.
 * @version 0.1
 * Creation Date  09-04-2007
 * Copyright IIITB
 */

/* History: 
 * ---------------------------------------------------------------------
 *      Date  |        Description of Change          |    Author      |
 * ---------------------------------------------------------------------
 *  09-04-07  |          Creation of File             |  Shreyas B.G.  |
 * ---------------------------------------------------------------------      
 */

package com.iiitb.scm.quadraticSolver;

import java.util.Vector;

import com.iiitb.scm.quadraticSolver.CplexQPSolver.SCMCplexException;


public class QuadMain {
	/**
	 * main() function used to test CplexQPSolver
	 * @param args
	 * @throws SCMCplexException
	 */
	public static void main(String[] args) throws SCMCplexException
	{
		try {
			/* Problem Type: 
			 * 1: Linear Obj Fn, Linear Constraints
			 * 2: Quad Obj Fn, Linear Constraints
			 * 3: Linear Obj Fn, Quad Constraints
			 * 4: Quad Obj Fn, Quad Constraints
			 */			
			int PROBLEM_TYPE = 2;
 
			/* Optimization Type:
			 * Minimization: 0
			 * Maximization: 1
			 */
			int OPT_TYPE = 0;
			
			// "Constraints" in CplexLPSolver has been replaced with optData here
			Vector<String> optData = new Vector<String>();

			// Add Objective Function
		/*	String objFunctionStr = "0.02 flowSF_1_0_0 + " + 
			                         "10 invfact_1_0 + " + 
			                         "0.02 flowFW_1_0_0 + " + 
			                         "10 invware_1_0 + " + 
			                         "0.02 flowWM_1_0_0 + " + 
			                         "10 invsup_1_0";

    		optData.add(objFunctionStr);

			// Add Linear Constraints and bounds
			optData.add("invsup_1_0 - flowSF_1_0_0 >= 0");
			optData.add("invfact_1_0 + flowSF_1_0_0 - flowFW_1_0_0 >= 0");
			optData.add("invware_1_0 + flowFW_1_0_0 - flowWM_1_0_0 >= 0");
			optData.add("0 <= invsup_1_0 <= 10000");
			optData.add("0 <= invfact_1_0 <= 8000");
			optData.add("0 <= invware_1_0 <= 7000");
			optData.add("15 <= flowSF_1_0_0 <= 8000");
			optData.add("15 <= invfact_1_0 <= 2000");
			optData.add("100 <= flowFW_1_0_0 <= 7000");
			optData.add("9 <= invware_1_0 <= 2000");
			optData.add("20 <= invsup_1_0 <= 10000");
			optData.add("10 <= flowWM_1_0_0 <= 7000");
			*/
			
			/* When integrating with GUI, only two lines of code have to be  
			 * included: 
			 * 1. Create object for Class CplexQPSolver (quadratic solver)
			 * 2. Call object.cplexSolve() method 
			 * The constraints and objective function would
			 * have been taken from the GUI and don't have to be created.
			 */ 
			// Create quadratic solver object 
			
			// /
			CplexQPSolver quadSolver = new CplexQPSolver(optData, 
					                                     PROBLEM_TYPE,
					                                     OPT_TYPE);
			// Call the cplexSolve() method in the solver object
			long startTime = System.currentTimeMillis();
			quadSolver.cplexSolve();
			long stopTime = System.currentTimeMillis();
			System.out.println("Execution time: " + (stopTime - startTime) + "mS");
			// */
           
			
            
			System.out.println("Quadratic Programming problem:\n");
			String filename = "C:\\Documents and Settings\\shreyas\\Desktop\\test.lp";
			//What is the iteration that needs to be sent here. Needs to be re evaluated.
			CplexQPSolver quadSolver2 = new CplexQPSolver(filename,2);
			long startTime1 = System.currentTimeMillis();
			quadSolver2.cplexSolve();
			long stopTime1 = System.currentTimeMillis();
			System.out.println("Execution time: " + (stopTime1 - startTime1) + "mS");
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}// End of main()

}// End of QuadMain class