package com.iitb.tspILProuting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.iiitb.DataExtraction;
import com.iiitb.GeodecDistance;
import com.iiitb.NodeDistance;
import com.iiitb.scm.graphview.graphstructure.Attribute;
import com.iiitb.scm.graphview.graphstructure.Edge;
import com.iiitb.scm.graphview.graphstructure.Node;

public class TSPILPDataPreprocessing {

	
	Vector edges = new Vector();
	Vector nodes = new Vector();
	static int numNodesNW1 =0;
	static int numNodesNW2 =0;
	Node globalSourceNode = null;
	HashMap nodeNames = new HashMap();
	int edgeID =0;
	
	
	public void dataExtraction(String attr1, String attr2, double attr3, double attr4,boolean isSource){
		DataExtraction de = new DataExtraction();
		de.extractData(attr1, attr2, attr3,
				attr4, isSource);
		List<NodeDistance> nodeDistances = de.getNodeDistances();
		List latitudes = de.getLats();
		List longitudes = de.getLongs();
		
		//Print node distances for testing
		int nodeID =0;
		Node sourceNode;
		Node destNode;
		for(int i =0;i<nodeDistances.size();i++){
			NodeDistance distanceEdge = nodeDistances.get(i);
			if(nodeNames.containsKey(distanceEdge.getStartNodeName()))
				sourceNode = (Node)nodeNames.get(distanceEdge.getStartNodeName());
			else
				{
					Node newNode = new Node();
					Attribute attr = new Attribute();
					newNode.setAttributes(attr);
					newNode.addAttribute("latitude", latitudes.get(nodeID).toString());
					newNode.addAttribute("longitude", longitudes.get(nodeID).toString());
					newNode.addAttribute("ID", distanceEdge.getStartNodeName());
					nodeID++;
					sourceNode = newNode;
					
					nodeNames.put(distanceEdge.getStartNodeName(),newNode);
					nodes.add(newNode);
					
				}
			
			
			if(nodeNames.containsKey(distanceEdge.getDestNodeName()))
				destNode = (Node)nodeNames.get(distanceEdge.getDestNodeName());
			else
				{
					Node newNode = new Node();
					Attribute attr = new Attribute();
					newNode.setAttributes(attr);
					newNode.addAttribute("latitude", latitudes.get(nodeID).toString());
					newNode.addAttribute("longitude", longitudes.get(nodeID).toString());
					newNode.addAttribute("ID", distanceEdge.getDestNodeName());
					nodeID++;
					destNode= newNode;
					nodeNames.put(distanceEdge.getDestNodeName(),newNode);
					nodes.add(newNode);
					
				}
			if (!distanceEdge.getStartNodeName().equals(distanceEdge.getDestNodeName()))
			{
				
					Edge edge = new Edge(sourceNode,destNode,edgeID);
					edge.addAttribute("distance", distanceEdge.getDistance().toString());
					edge.addAttribute("name", distanceEdge.getStartNodeName()+"_"+distanceEdge.getDestNodeName());
					edges.add(edge);
					edgeID++;
					System.out.print(distanceEdge.getStartNodeName() +",");
					System.out.print(distanceEdge.getDestNodeName() + ",");
					System.out.print(distanceEdge.getDistance()+",");
					double dist = distanceEdge.getDistance();
					System.out.println((dist/20)*60);
					System.out.println();
			}			
			
		}// End of for
		
		if(isSource){
			numNodesNW1= nodeNames.size();
			System.out.println("No of nodes in NW1:" + numNodesNW1);
		}
		else {
			numNodesNW2 = nodeNames.size()-numNodesNW1;
			System.out.println("No of nodes in NW2:"+ numNodesNW2);
			GeodecDistance gd = new GeodecDistance().getInstance();
			
			//Code for getting road distance using Google API
			List<String> originList = new ArrayList<String>();
			List<String> destinationList = new ArrayList<String>();
			
			for (int i = 1; i < numNodesNW1; i++) {
					for (int k = numNodesNW1; k < nodes.size(); k++) {
						
						Node node1 = (Node)nodes.get(i);
						Node node2 = (Node)nodes.get(k);
						if(!node1.getAttributeValue("ID").equals(node2.getAttributeValue("ID")))
						{
						
						originList.add( node1.getAttributeValue("latitude")+","+node1.getAttributeValue("longitude"));
						destinationList.add(node2.getAttributeValue("latitude")+","+node2.getAttributeValue("longitude"));
						}
					}
			}
			
			ArrayList<ArrayList<String>> dis= new ArrayList<ArrayList<String>>();
			
			String[] origins = new String[originList.size()];
			origins = originList.toArray(origins);
			String[] destinations = new String[destinationList.size()];
			destinations = destinationList.toArray(destinations);
			ArrayList<ArrayList<String>> result = 
					gd.getGoogleAPIRoadDistance(originList,origins,destinationList,destinations,0,dis);
			//End of Google API Usage
		
			for(int i=0;i<result.size();i++)
			{
				
				ArrayList<String> temp = result.get(i);
				System.out.println("Origin: "+temp.get(0) + " Destination: "+temp.get(1)+ " Distance: "+temp.get(2));
			}
			
			//End of Google API call
			int resultCount=0;
			for(int l=1;l<numNodesNW1;l++){
				for(int k=numNodesNW1;k<nodes.size();k++){
					Node node1 = (Node)nodes.get(l);
					Node node2 = (Node)nodes.get(k);
					if(!node1.getAttributeValue("ID").equals(node2.getAttributeValue("ID")))
					{
						Edge edge = new Edge(node1,node2,edgeID);
						/*System.out.println("================"+Double.parseDouble(node1.getAttributeValue("latitude")));
						System.out.println(Double.parseDouble(node1.getAttributeValue("longitude")));
						System.out.println(Double.parseDouble(node2.getAttributeValue("latitude")));
						System.out.println(Double.parseDouble(node2.getAttributeValue("longitude")));*/
						
						ArrayList<String> temp1 = result.get(resultCount);
						String distance = temp1.get(2);
						int kmFlag=0;
						if(distance.contains("km"))
							kmFlag=1;
						else
							kmFlag=0;
						double dist = Double.parseDouble(distance.substring(0, distance.indexOf(' ')));
						if(kmFlag==0)
							dist=dist/1000;
						/*Double distance = gd.getGeodecDistance(Double.parseDouble(node1.getAttributeValue("latitude")), Double.parseDouble(node1.getAttributeValue("longitude")),
									//Double.parseDouble(node2.getAttributeValue("latitude")), Double.parseDouble(node2.getAttributeValue("longitude")));*/
						edge.addAttribute("distance",String.valueOf(dist));
						edge.addAttribute("name", node1.getAttributeValue("ID")+"_"+node2.getAttributeValue("ID"));
						System.out.println(node1.getAttributeValue("ID") + "," + node2.getAttributeValue("ID") + ","+ dist+ ","+((dist/20)*60));
						edges.add(edge);
						edgeID++;
						resultCount++;
					}
				}//End of for
				
			}//End of for
		}//end of if
	}
	
	public Vector getEdges() {
		return edges;
	}
	public Vector getNodes() {
		return nodes;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TSPILPDataPreprocessing runObject = new TSPILPDataPreprocessing();
		runObject.dataExtraction("1287YP(23)", "S3",13.02886,
				77.5335698,true);
		runObject.dataExtraction("1287YP(23)", "S4",13.02886,
				77.5335698,false);
	}

}
