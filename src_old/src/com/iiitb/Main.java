package com.iiitb;

import com.google.maps.GeoApiContext;
import com.google.maps.model.*;
import com.google.maps.*;
public class Main {

    public static void main(String[] args) {
        try {
            GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyCAYx1F8UtCIrVsR-CEkwe2xf3GueDFtis");
            String[] origins = new String[] {
                    "13.02886,77.5335698", "kormangala, bangalore"
            };
            String[] destinations = new String[] {
                    "12.971599,77.594563", "jaynagar,bangalore"
            };
            DistanceMatrix matrix =
                    DistanceMatrixApi.getDistanceMatrix(context, origins, destinations).await();
            System.out.println("Distance from hsr to iiitb: "+matrix.rows[0].elements[0].distance);
            System.out.println("Distance from kormangala to jaynagar: "+matrix.rows[1].elements[0].distance);

        }
        catch (Exception e){

        }
	// write your code here
        
    }
}
