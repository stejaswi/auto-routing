/*
 * Created on Jul 14, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.iiitb.scm.graphview.graphstructure;
import java.util.*;

/**
 * @author Harjinder
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
@SuppressWarnings("unchecked")
public class Attribute {
	@SuppressWarnings("unchecked")
	private HashMap hmAttrributes = null;
	
	@SuppressWarnings("unchecked")
	public Attribute(){
		hmAttrributes = new HashMap();
	}
	
	/**********Many times it is required to have a copy of the Attribute instance...***********/
	//get the instance of Attribute, given a hash map...
	//used for the getCopy()
	@SuppressWarnings("unchecked")
	public Attribute(HashMap hmAttr){
		this.hmAttrributes = hmAttr;
	}
	//get the current hash map of the instance...
	//used by the getCopy()
	public HashMap getHashMap(){
		return this.hmAttrributes;
	}
	
	//get the Copy of the Attribute instance...
	public static Attribute getCopy(Attribute attr){
		Attribute result = null;
		//have a new intance of a hash map...
		HashMap tgt = new HashMap(attr.getHashMap());
		//get the new instance of the Attribute...
		result = new Attribute(tgt);
		return result;
	}
	
	public void addAttribute(String strName, String strValue){
		this.hmAttrributes.put(strName, strValue);
	}

	@SuppressWarnings("unused")
	
	void updateWithOverWrite(Attribute attr){
		Set keySet = attr.hmAttrributes.keySet();
		String strName=null, strValue=null;
		
		Iterator iter = keySet.iterator();
		while(iter.hasNext()){
			strName = (String)iter.next();
			this.hmAttrributes.put(strName, attr.getValue(strName));
		}
	}
	@SuppressWarnings("unused")
	
	void updateWithoutOverWrite(Attribute attr){
		Set keySet = attr.hmAttrributes.keySet();
		String strName=null, strValue=null;
		
		Iterator iter = keySet.iterator();
		while(iter.hasNext()){
			strName = (String)iter.next();
			if(this.getValue(strName)==null)
				this.hmAttrributes.put(strName, attr.getValue(strName));
		}
	}
	
	String getValue(String strName){
		return ((String)this.hmAttrributes.get(strName));
	}
	
	void remove(String strName){
		this.hmAttrributes.remove(strName);
	}
	
	void update(String strName, String strNewValue){
		this.hmAttrributes.remove(strName);
		this.hmAttrributes.put(strName, strNewValue);
	}
	
	public void reset(String strAll){
		this.hmAttrributes.clear();
		if(strAll.length() == 0)
			return;
		String[] strAttr = strAll.split("\n");
		String[] strKeyValue = null;
		for(int i=0; i<strAttr.length; i++){
			strKeyValue = strAttr[i].split("=");
			if(strKeyValue.length>=2)
				hmAttrributes.put(strKeyValue[0], strKeyValue[1]);
		}
	}
	
	public String getString(){
		String strAttr = hmAttrributes.toString();
		if(strAttr!=null){
			strAttr = strAttr.replaceAll(", ","\n");
			strAttr = strAttr.replace('{',' ');
			strAttr = strAttr.replace('}',' ');
			strAttr = strAttr.trim();
		}
		return strAttr;
	}
}
