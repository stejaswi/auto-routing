package com.iiitb;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


 
/**
 * FileName: CSVtoAL.java
 * 
 * @author Sunil
 * @date of creation Jul 22, 2015
 * @Copyright: International Institute of Information Technology, Bangalore,
 *             India (IIITB). All rights Reserved.
 * @description The class does CSV to ArrayList conversion and search all possible path searches
 * 
 */
 
public class CSVtoAL {   

	
	public static void main(String[] args) {
		
		BufferedReader tempBuffer = null;
		List<NodeDistance> nodeDistances = new ArrayList<NodeDistance>();
		ArrayList<String> nodeArray = new ArrayList<String>();
		String UTF8_BOM = "\uFFFD";
		
		DataExtraction de = new DataExtraction();
		try{
			de.readData();
			//de.computeDistances("1287YP(23)","S3",13.02886,77.5335698);
		}
		catch(Exception ae){
			ae.printStackTrace();
		}
		
		try {
			String linebyLine;
			tempBuffer = new BufferedReader(new FileReader("data/S3-C6.txt"));
			
			// How to read file in java line by line?
			while ((linebyLine = tempBuffer.readLine()) != null) {
				System.out.println("CSV data: " + linebyLine);
				nodeArray = CSVtoArrayList(linebyLine);
				//Converting arraylist to array
				String[] simpleArray = nodeArray.toArray(new String[nodeArray.size()]);
				System.out.println("Converted ArrayList data:" + simpleArray[0].toString() + "\n");
				String source = simpleArray[0].toString();
				String dest = simpleArray[1].toString();
				//Remove unwanted characters
				if (source.startsWith(UTF8_BOM))
		        {
					source=source.replace(UTF8_BOM, "");
		        }
				if (source.endsWith("﻿s")) source = "s"; 
				//String nodeName = simpleArray[0].toString()+simpleArray[1].toString();
				//String node = "nodeDistance"+nodeName;
				NodeDistance nodeDistanceSA = new NodeDistance(source,dest,Double.valueOf(simpleArray[2]));
				nodeDistances.add(nodeDistanceSA);
			}
			System.out.println("nodeDistances: " + nodeDistances.size());
			
			// Call for all possible combinations
			FindPossiblePath findPossiblePath = new FindPossiblePath();
			// Starting point is "s"-hub
			findPossiblePath.findPossiblePath(nodeDistances, "s");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (tempBuffer != null) tempBuffer.close();
			} catch (IOException exception) {
			 exception.printStackTrace();
			}
		}
	}
	
	// Utility which converts CSV to ArrayList using Split Operation
	public static ArrayList<String> CSVtoArrayList(String inputCSV) {
		ArrayList<String> node = new ArrayList<String>();
	
		//comment
		if (inputCSV != null) {
			String[] splitData = inputCSV.split("\t");
			for (int i = 0; i < splitData.length; i++) {
				if (!(splitData[i] == null) || !(splitData[i].length() == 0)) {
					node.add(splitData[i].trim());
				}
			}
		}
		return node;
	}
	
}