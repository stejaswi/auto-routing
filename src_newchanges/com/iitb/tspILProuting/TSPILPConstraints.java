package com.iitb.tspILProuting;

import java.util.Vector;

import com.iiitb.scm.graphview.graphstructure.Edge;
import com.iiitb.scm.graphview.graphstructure.Node;

public class TSPILPConstraints implements ILPProblem {

	Vector nodes = null;
	Vector edges = null;
	String objectiveType = null;
	String objective = "";
	Vector constraints = null;
	Vector ILPVariables = null;
	Vector binaryVariables = null;

	int NW1size=0;
	int NW2size=0;
	
	
	public void populateInputNetwork(Vector nodes,Vector edges,int numNodesNW1,int numNodesNW2){
		
		this.nodes = nodes;
		this.edges = edges;
		this.NW1size = numNodesNW1;
		this.NW2size = numNodesNW2;
		
	}
	
		
	public void createILPModel(){
		
		createObjectiveFunction();
		createConstraints();
		//createBinaryVariables();
		createILPVariables();
	}
	
	@Override
	public String createObjectiveFunction() {
		
		setObjectiveType("Minimize");
		for(int i=0;i<this.edges.size()-1;i++)
		{ 
			Edge e = (Edge)edges.get(i);
			objective= objective + e.getAttributeValue("distance")+" "+ e.getAttributeValue("name")+ " + ";
			
		}
		Edge edge = (Edge)edges.get(this.edges.size()-1);
		objective = objective +  edge.getAttributeValue("distance")+" "+ edge.getAttributeValue("name");
		
		System.out.println("Objective function is :"+ objective);
		
		return objective;
	}

	


	@Override
	public Vector createConstraints() {
		
		constraints = new Vector();
		// Only one outgoing arc from a node to every other node in network1
		
		int k=0;
		Node nodeSource= (Node)nodes.get(k);
		StringBuffer constraint1 = new StringBuffer();
		for(int j=1; j<NW1size;j++)
		{
			Node node2 =(Node)nodes.get(j);
			constraint1.append(nodeSource.getAttributeValue("ID")+"_"+node2.getAttributeValue("ID") + " + ");
		}
		constraint1.deleteCharAt(constraint1.length()-2);
		constraint1.append(" = 1");
						
		constraints.add(constraint1.toString());
		System.out.println(constraint1);
		
		for(int i=1;i<NW1size;i++)
		{
			Node node1= (Node)nodes.get(i);
			StringBuffer constraint = new StringBuffer();
			for(int j=1;j<nodes.size();j++)
			{
				if(i!=j)
				{
					Node node2 =(Node)nodes.get(j);
					constraint.append(node1.getAttributeValue("ID")+"_"+node2.getAttributeValue("ID") + " + ");
				}		
			}
			
			constraint.deleteCharAt(constraint.length()-2);
			constraint.append(" = 1");
							
			constraints.add(constraint.toString());
			System.out.println(constraint);
		}
		
		//Only one outgoing arc from a node to every other node in network 2
		for(int i=NW1size;i<nodes.size();i++)
		{
			Node node1= (Node)nodes.get(i);
			StringBuffer constraint = new StringBuffer();
			for(int j=NW1size;j<nodes.size();j++)
			{
				if(i!=j)
				{
					Node node2 =(Node)nodes.get(j);
					constraint.append(node1.getAttributeValue("ID")+"_"+node2.getAttributeValue("ID") + " + ");
				}
				
						
			}
			Node destination = (Node)nodes.get(0);
			constraint.append(node1.getAttributeValue("ID")+"_"+ destination.getAttributeValue("ID"));
			constraint.append(" = 1");
			constraints.add(constraint.toString());
			System.out.println(constraint);
		}
		
		
		// Only one incoming arc from every node to a node in network1
				for(int j=1;j<NW1size;j++)
				{
					Node node1= (Node)nodes.get(j);
					StringBuffer constraint = new StringBuffer();
					for(int i=0;i<NW1size;i++)
					{
						if(i!=j)
						{
							Node node2 =(Node)nodes.get(i);
							constraint.append(node2.getAttributeValue("ID")+"_"+node1.getAttributeValue("ID") + " + ");
						}
							
					}
					constraint.deleteCharAt(constraint.length()-2);
					constraint.append(" = 1");
					constraints.add(constraint.toString());
					System.out.println(constraint);
				}
				
				//Only one incoming arc from every node to a node in network 2
				for(int j=NW1size;j<nodes.size();j++)
				{
					Node node1= (Node)nodes.get(j);
					StringBuffer constraint = new StringBuffer();
					for(int i=1;i<nodes.size();i++)
					{
						if(i!=j)
						{
							Node node2 =(Node)nodes.get(i);
							constraint.append(node2.getAttributeValue("ID")+"_"+node1.getAttributeValue("ID") + " + ");
									
						}
						
					}
					if(constraint.length()!=0)
					{
						constraint.deleteCharAt(constraint.length()-2);
						constraint.append(" = 1");
						constraints.add(constraint.toString());
						System.out.println(constraint);
					}
						
				}
				
				//An arc exists from every node in network 2 to the source which
				//is the  destination in this case
				
			/*	Node node2 = (Node)nodes.get(0);
				String constraint ="";
				for(int j=NW1size;j<nodes.size()-1;j++){
					Node node1= (Node)nodes.get(j);
					
					constraint = constraint + node1.getAttributeValue("ID")+"_"+node2.getAttributeValue("ID") + " + ";	
				}
				Node node1 = (Node)nodes.get(nodes.size()-1);
				constraint = constraint + node1.getAttributeValue("ID")+"_"+node2.getAttributeValue("ID") + " = 1 ";
				constraints.add(constraint);
				System.out.println(constraint); */
				
				
				//No sub tour exists for network 1
				
				for(int i=1;i<NW1size;i++)
				{
					Node subTourNode1= (Node)nodes.get(i);
					
					for(int j=1;j<NW1size;j++)
					{
						if (i != j)
						{	Node subTourNode2 =(Node)nodes.get(j);
							String subTourConstraint = "";
							subTourConstraint = subTourConstraint + "u_" + subTourNode1.getAttributeValue("ID")+" - "+"u_"+subTourNode2.getAttributeValue("ID") + " + " +
							NW1size + " " + subTourNode1.getAttributeValue("ID") + "_" + subTourNode2.getAttributeValue("ID") +  " <= " + (NW1size-1);
							constraints.add(subTourConstraint);
							System.out.println(subTourConstraint);
						}
								
					}		
					
				}
				
				
				//No sub tour exists for network 2
				
				for(int i=NW1size;i<nodes.size();i++)
				{
					Node subTourNode1= (Node)nodes.get(i);
					
					for(int j=NW1size;j<nodes.size();j++)
					{
						if (i != j)
						{	Node subTourNode2 =(Node)nodes.get(j);
							String subTourConstraint = "";
							subTourConstraint = subTourConstraint + "u_" + subTourNode1.getAttributeValue("ID")+" - "+"u_"+subTourNode2.getAttributeValue("ID") + " + " +
							NW2size + " " + subTourNode1.getAttributeValue("ID") + "_" + subTourNode2.getAttributeValue("ID") +  " <= " + (NW2size-1);
							constraints.add(subTourConstraint);
							System.out.println(subTourConstraint);
						}
								
					}
					
					
				}
				
		//Adding bounds for edge variables ( They are either 0 or 1)
		for (int i=0;i< edges.size();i++){
			Edge e = (Edge)edges.get(i);
			constraints.add(e.getAttributeValue("name") + " >= 0");
			constraints.add(e.getAttributeValue("name") + " <= 1");
			
			System.out.println(e.getAttributeValue("name") + " >= 0");
			System.out.println(e.getAttributeValue("name") + " <= 1");
		}	
		
		/*for(int i=1;i<nodes.size();i++)
		{
			Node n = (Node)nodes.get(i);
			constraints.add("u_"+n.getAttributeValue("ID") + ">= 0");
			System.out.println("u_"+n.getAttributeValue("ID") + ">= 0");
					
		}*/
		
		return constraints;
	}

	@Override
	public Vector createBounds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vector createILPVariables() {
		
		ILPVariables = new Vector();
		System.out.println("ILP Variables:");
		for(int i=1;i<nodes.size();i++)
		{
			Node n = (Node)nodes.get(i);
			ILPVariables.add("u_"+n.getAttributeValue("ID"));
			System.out.println("u_"+n.getAttributeValue("ID"));
					
		}
		
		for (int i=0;i< edges.size();i++){
			Edge e = (Edge)edges.get(i);
			ILPVariables.add(e.getAttributeValue("name"));
			System.out.println(e.getAttributeValue("name"));
		}	
		return ILPVariables;
	}
	
	public String getObjectiveType() {
		return objectiveType;
	}


	public void setObjectiveType(String objectiveType) {
		this.objectiveType = objectiveType;
	}

	@Override
	public Vector createBinaryVariables() {
		
		binaryVariables = new Vector();
		System.out.println("Binary Variables:");
		for (int i=0;i< edges.size();i++){
			Edge e = (Edge)edges.get(i);
			binaryVariables.add(e.getAttributeValue("name"));
			System.out.println(e.getAttributeValue("name"));
		}
		return binaryVariables;
	}

	public String getObjective() {
		return objective;
	}

	public Vector getConstraints() {
		return constraints;
	}

	public Vector getILPVariables() {
		return ILPVariables;
	}

	public Vector getBinaryVariables() {
		return binaryVariables;
	}

	
	
}
