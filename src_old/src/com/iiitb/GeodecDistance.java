package com.iiitb;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;

public class GeodecDistance {
	
	GeodecDistance obj = null;
	DistanceMatrix matrix;
	public GeodecDistance(){
		
		
	}
	
	public GeodecDistance getInstance(){
		if(this.obj==null)
			obj = new GeodecDistance();
			
		return this.obj;
	}
	
	/**
	 * Method to Calculate the geodec distances between
	 * two latitudes and longitudes.
	 */
	
	public static double getGeodecDistance(double lat1, double long1, double lat2, double long2){
		System.out.println("Inside geodec distance calculation");
		double earthRadius = 6371000; // Radius in meters
	    double latitude = Math.toRadians(lat2-lat1);
	    double longitude = Math.toRadians(long2-long1);
	    double a = Math.sin(latitude/2) * Math.sin(latitude/2) +
	               Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
	               Math.sin(longitude/2) * Math.sin(longitude/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double distance = (double) ((earthRadius * c)/1000);
	    return distance;
	   //return( Math.round(distance*100)/100.0d);
	    }
	
	
	public ArrayList<ArrayList<String>> getGoogleAPIRoadDistance
	(List<String> oL,String[] origins, 
			List<String> dL, String[] destinations,
			int index,ArrayList<ArrayList<String>> dis) {
		//Double dist = 0.0;
		System.out.println();
		System.out.println(oL + " " + origins + " " +  dL + " " + destinations);
		try {
			//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBITksYO60Fld9ea_dr3N1s0SaAWfeTnUc");
			GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyCAYx1F8UtCIrVsR-CEkwe2xf3GueDFtis");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			//System.out.println("Reached here");
			for(int i=0;i< oL.size()/2;i++) {
				String[] oSL = new String[oL.size()];
				oSL = oL.subList(i, i+1).toArray(oSL);
				String[] dSL = new String[dL.size()];
				dSL = dL.toArray(dSL);//entire column
		
				matrix = DistanceMatrixApi.getDistanceMatrix(context,oSL,dSL).await();
				for(int j=0;j<dL.size()/2;j++)
				{
					System.out.println("**** " + dL.size() + "Distance " + matrix.rows[i].elements[j].distance);

					String distance = matrix.rows[i].elements[j].distance.toString();
					ArrayList<String> temp = new ArrayList<String>();
					temp.add(origins[i]);
					temp.add(destinations[j]);
					temp.add(distance);
					dis.add(temp);
					//System.out.println("Source: "+smallerOrigins[k] + " Destination: "+smallerDestinations[k] + " Distance: " + distance);
				}
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		catch (Exception e){
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			//getGoogleAPIRoadDistance(origins,destinations,i,dis);

			//System.out.println("Exception in Google API usage:");
			//e.printStackTrace();
		}
		return dis;
	}
	
	
	
	public ArrayList<ArrayList<String>> getGoogleAPIRoadDistance1(String[] origins, String[] destinations,int index,ArrayList<ArrayList<String>> dis){
		 //Double dist = 0.0;
		 int i=0;
		 boolean flag = false;
		 try {
				 String [] smallerOrigins = new String[2];
				 String [] smallerDestinations = new String[2];
				 
				 
				 int j=0;
				 for(i=index;i<(index+2) && i<origins.length;i++)
			 	 {
					flag = true;
			 		smallerOrigins[j]= origins[i];
			 		smallerDestinations[j]=destinations[i];
			 		j++;
			 		//System.out.println("Source: "+origins[i] + " Destination: "+destinations[i]);
			 	 }
				 
			 	
			 	//System.out.println(origins.length + " "+destinations.length);
	            if(flag)
	            {
	            	
	            	//GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBITksYO60Fld9ea_dr3N1s0SaAWfeTnUc");
	            	GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyCAYx1F8UtCIrVsR-CEkwe2xf3GueDFtis");
	            	try {
						Thread.sleep(5000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
	            	matrix = DistanceMatrixApi.getDistanceMatrix(context, smallerOrigins, smallerDestinations).await();
	            	
	            	int count = 0;
	            	//System.out.println("Reached here");
	            	for(int k=0;k<j;k++)
	            	{
	            		
	            		String distance = matrix.rows[count].elements[0].distance.toString();
	            		ArrayList<String> temp = new ArrayList<String>();
	            		temp.add(smallerOrigins[k]);
	            		temp.add(smallerDestinations[k]);
	            		temp.add(distance);
	            		
	            		dis.add(temp);
	            		
	            		//System.out.println("Source: "+smallerOrigins[k] + " Destination: "+smallerDestinations[k] + " Distance: " + distance);
	            		count++;
	            	}
	            	getGoogleAPIRoadDistance1(origins,destinations,i,dis);
	            } 
	            
	        }
	        catch (Exception e){
	        	try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	        	e.printStackTrace();
	        	//getGoogleAPIRoadDistance(origins,destinations,i,dis);
	        	
	        	//System.out.println("Exception in Google API usage:");
	        	//e.printStackTrace();
	        }
		 
		 return dis;
	}

	public double getGoogleAPIDistanceValues(int k){
		
		System.out.println("Distance using Google API: "+matrix.rows[k].elements[0].distance);
        String distance = matrix.rows[k].elements[0].distance.toString();
        distance = distance.substring(0, distance.indexOf(' '));
        return(Double.parseDouble(distance));
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		GeodecDistance gd = new GeodecDistance();
		double distance = gd.getGeodecDistance(13.02886, 77.53356980000001, 13.05081134, 77.59931527999993);
		System.out.println(distance);
		
	}



	

}
